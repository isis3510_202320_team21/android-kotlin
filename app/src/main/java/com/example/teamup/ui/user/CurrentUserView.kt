package com.example.teamup.ui.user

/**
 * User details post authentication that is exposed to the UI
 */
data class CurrentUserView(
    val displayName: String,
    val email: String,
    val meetings_7days: Int
    //... other data fields that may be accessible to the UI
)