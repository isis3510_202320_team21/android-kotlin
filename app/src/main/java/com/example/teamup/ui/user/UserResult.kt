package com.example.teamup.ui.user

/**
 * Authentication result : success (user details) or error message.
 */
data class UserResult(
    val success: CurrentUserView? = null,
    val error: Int? = null
)