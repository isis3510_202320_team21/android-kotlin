package com.example.teamup.ui.createTeam

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.teamup.ConnectionLiveData
import com.example.teamup.R
import com.example.teamup.databinding.SearchStagesLayoutBinding
import com.google.android.gms.common.api.GoogleApi
import com.google.android.gms.common.api.internal.GoogleApiManager
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SearchStagesFragment : Fragment() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var _binding: SearchStagesLayoutBinding? = null
    private val binding get() = _binding!!
    private var meetlocation: LatLng? = null
    private lateinit var map: GoogleMap
    private lateinit var locationManager: LocationManager
    private var gpsStatus = false
    //private val createTeamViewModel: CreateTeamViewModel by activityViewModels()
    private lateinit var createTeamViewModel: CreateTeamViewModel
    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog


    private val permissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            showLocation()
        }
    }
    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        showLocation()
        if (createTeamViewModel.cache["latitude"] != ""){
            newMarker(LatLng(createTeamViewModel.cache["latitude"]!!.toDouble(),createTeamViewModel.cache["longitude"]!!.toDouble() ))
        }
        map.setOnMapClickListener {
            newMarker(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SearchStagesLayoutBinding.inflate(inflater, container, false)


        //Network checking
        checkNetworkConnection()
        val appContext = context?.applicationContext
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.weather_label)
        //set message for alert dialog
        builder.setMessage("You are currently offline, check your internet connection to continue")
        builder.setIcon(R.drawable.baseline_wifi_off_24)
        builder.setPositiveButton("Go back"){dialogInterface , which ->
            activity?.onBackPressed()
        }
        // Create the AlertDialog
        alertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)

        if (!checkForInternet(appContext!!)) {
            Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
            alertDialog.show()
        }

        val auth = Firebase.auth
        auth.currentUser?.reload()


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createTeamViewModel = ViewModelProvider(requireActivity(), CreateTeamViewModelFactory())
            .get(CreateTeamViewModel::class.java)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(callback)

        binding.confirmBtn.setOnClickListener {
            createTeamViewModel.createTeam()
             findNavController().navigate(R.id.action_searchStages_toTeam) }
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
    }

    private fun showLocation() {
        if (ContextCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            } else {

                permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }
        if (ContextCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
        }
    }



    private fun checkGpsStatus() {
        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun newMarker(p0: LatLng) {
        meetlocation = p0
        map.clear()
        map.addMarker(MarkerOptions().position(p0).title("meeting point"))
        createTeamViewModel.setTeamLatLng(meetlocation?.latitude.toString(), meetlocation?.longitude.toString())

    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}