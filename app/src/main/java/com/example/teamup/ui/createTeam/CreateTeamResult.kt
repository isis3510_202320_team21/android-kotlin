package com.example.teamup.ui.createTeam

/**
 * Authentication result : success (user details) or error message.
 */
data class CreateTeamResult(
    val success: CreateTeamView? = null,
    val error: Int? = null
)