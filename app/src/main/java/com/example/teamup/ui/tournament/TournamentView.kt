package com.example.teamup.ui.tournament

/**
 * User details post authentication that is exposed to the UI
 */
data class TournamentView(
    val tournament: String,
    //... other data fields that may be accessible to the UI
)