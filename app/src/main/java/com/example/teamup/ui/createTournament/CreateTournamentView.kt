package com.example.teamup.ui.createTournament

data class CreateTournamentView (
    val name:String,
    val sport:String,
    val date:String,
    val location: String,
    val numTeams: String
)