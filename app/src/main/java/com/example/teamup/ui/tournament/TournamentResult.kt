package com.example.teamup.ui.tournament

/**
 * Authentication result : success (user details) or error message.
 */
data class TournamentResult(
    val success: Any? = null,
    val error: Int? = null
)