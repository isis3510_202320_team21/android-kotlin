package com.example.teamup.ui.tournament

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.teamup.ConnectionLiveData
import com.example.teamup.MyItemRecyclerViewAdapter
import com.example.teamup.R
import com.example.teamup.TournamentItemRecyclerViewAdapter
import com.example.teamup.data.model.Tournament
import com.example.teamup.databinding.FragmentTournamentsListBinding
import com.example.teamup.ui.tournament.TournamentViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*
import kotlin.collections.ArrayList


/**
 * A fragment representing a list of Items.
 */
@OptIn(InternalCoroutinesApi::class)
class TournamentsFragment : Fragment() {

    private lateinit var tournamentListenerRegistration: ListenerRegistration

    private var columnCount = 1
    private var _binding: FragmentTournamentsListBinding? = null
    private val binding get() = _binding!!

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionId = 2

    private var lon = ""
    private var lat = ""
    private var msg = ""

    private lateinit var tournamentViewModel: TournamentViewModel

    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    private var track_pressed:Boolean = false
    val cache:HashMap <String, String?> = hashMapOf("tour" to null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Network checking
        checkNetworkConnection()
        val appContext = context?.applicationContext
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.team_up_label)
        //set message for alert dialog
        builder.setMessage("You are currently offline. Be aware that the list may not be updated.")
        builder.setIcon(R.drawable.baseline_wifi_off_24)
        builder.setPositiveButton("Ok"){dialogInterface , which ->
        }
        // Create the AlertDialog
        alertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)

        _binding = FragmentTournamentsListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val popular = sharedPref.getString("popularTournament","No popular tournament yet")


        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.team_up_label)
        //set message for alert dialog
        builder.setMessage("The most popular tournament is "+popular)
        builder.setIcon(R.drawable.baseline_emoji_events_24)
        builder.setPositiveButton("Ok"){dialogInterface , which ->
        }
        // Create the AlertDialog
        var alertDialog2: AlertDialog
        alertDialog2 = builder.create()
        // Set other dialog properties
        alertDialog2.setCancelable(false)
        alertDialog2.setCanceledOnTouchOutside(true);
        alertDialog2.show()

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        tournamentViewModel = ViewModelProvider(this, TournamentViewModelFactory())
            .get(TournamentViewModel::class.java)
        tournamentViewModel.get()

        var lista: ArrayList<Tournament> = ArrayList()

        tournamentViewModel.tournamentsResult.observe(viewLifecycleOwner,
            Observer { trackResult ->
                trackResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                trackResult.error?.let {
                    showRegisterFailed(it)
                }
                trackResult.success?.let {
                    // Set the adapter
                    lista = it as ArrayList<Tournament>
                    updateUiWithUser(it as ArrayList<Tournament>)

                }
            })

        tournamentViewModel.tournamentsTrackResult.observe(viewLifecycleOwner,
            Observer { trackResult ->
                trackResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                trackResult.error?.let {
                    showRegisterFailed(it)
                }
                trackResult.success?.let {
                    val tour = it as TournamentView
                    val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
                    with (sharedPref!!.edit()) {
                        putString("popularTournament",it.tournament)
                        apply()
                    }
                }
            })

        binding.createTournamentBtn.setOnClickListener { findNavController().navigate(R.id.CreateTournament)}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }



    }

    private fun updateUiWithUser(model: ArrayList<Tournament>) {
        val appContext = context?.applicationContext ?: return

        Log.i("SSS", model.size.toString())
        if(model.size<1){

            binding.nolist.visibility = View.VISIBLE
            binding.list.visibility = View.GONE
        }
        else{
            binding.nolist.visibility = View.GONE
            binding.list.visibility = View.VISIBLE
        }

        val recyclerView:RecyclerView = binding.list


        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                var adapter = TournamentItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : TournamentItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        track_pressed = true
                        if(checkForInternet(appContext!!)){
                            tournamentViewModel.track(model[position].name.toString())
                            track_pressed = false
                            cache["tour"]=null
                            val builder = AlertDialog.Builder(requireActivity())
                            //set title for alert dialog
                            builder.setTitle(model[position].name.toString())
                            //set message for alert dialog
                            var message = "Remember your "+model[position].sport.toString()+" tournament in "+ model[position].location.toString()+"!"+
                                    "\n"+ "There are "+model[position].numTeams.toString()+" teams in this tournament."
                            builder.setIcon(R.drawable.munequito)

                            //performing positive action
                            //performing cancel action
                            builder.setPositiveButton("Ok"){dialogInterface , which ->

                            }
//                        builder.setNegativeButton("Delete tournament") { dialogInterface, which ->
//                            tournamentViewModel.removeTournament(model[position].name.toString())
//                        }

                            builder.setMessage(message)
                            val alertDialog: AlertDialog = builder.create()
                            // Set other dialog properties
                            alertDialog.setCancelable(false)
                            alertDialog.setCanceledOnTouchOutside(true);
                            alertDialog.show()
                        }
                        else{
                            cache["tour"] = model[position].name.toString()
                            binding.loading.visibility = View.GONE
                            val builder = AlertDialog.Builder(requireActivity())
                            //set title for alert dialog
                            builder.setTitle(model[position].name.toString())
                            //set message for alert dialog
                            var message = "Remember your "+model[position].sport.toString()+" tournament in "+ model[position].location.toString()+"!"+
                                    "\n"+ "There are "+model[position].numTeams.toString()+" teams in this tournament."
                            builder.setIcon(R.drawable.munequito)

                            //performing positive action
                            //performing cancel action
                            builder.setPositiveButton("Ok"){dialogInterface , which ->

                            }
//                        builder.setNegativeButton("Delete tournament") { dialogInterface, which ->
//                            tournamentViewModel.removeTournament(model[position].name.toString())
//                        }

                            builder.setMessage(message)
                            val alertDialog: AlertDialog = builder.create()
                            // Set other dialog properties
                            alertDialog.setCancelable(false)
                            alertDialog.setCanceledOnTouchOutside(true);
                            alertDialog.show()
                        }
                    }
                })
            }
        }
    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
                if(track_pressed){
                    if (cache["tour"]!=null){
                        tournamentViewModel.trackBack(cache["tour"]!!)
                        cache["tour"]=null
                        track_pressed = false
                    }
                }
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }


   @SuppressLint("MissingPermission", "SetTextI18n")
   @Synchronized
    private fun getLocation() {
        val appContext = context?.applicationContext ?: return
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    val location: Location? = task.result
                    if (location != null) {
                        lon = location.longitude.toString()
                        lat = location.latitude.toString()
                        val geocoder = Geocoder(appContext, Locale.getDefault())
                        val list: List<Address> =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1) as List<Address>
                    }
                }
            } else {
                Toast.makeText(appContext, "Please turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }
    private fun isLocationEnabled(): Boolean {
        val appContext = context?.applicationContext ?: return false
        val locationManager: LocationManager =
            appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }
    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == permissionId) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLocation()
            }
        }
    }

    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        //Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TournamentsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}