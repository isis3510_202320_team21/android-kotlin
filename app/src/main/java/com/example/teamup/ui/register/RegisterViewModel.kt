package com.example.teamup.ui.register

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.example.teamup.data.LoginRepository
import com.example.teamup.data.Result

import com.example.teamup.R
import com.example.teamup.ui.login.LoggedInUserView
import com.example.teamup.ui.login.LoginResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegisterViewModel(private val registerRepository: LoginRepository) : ViewModel() {

    private val _registerForm = MutableLiveData<RegisterFormState>()
    val registerFormState: LiveData<RegisterFormState> = _registerForm

    private val _registerResult = MutableLiveData<RegisterResult>()
    val registerResult: LiveData<RegisterResult> = _registerResult

    fun register(username: String, firstName: String, lastName: String, email: String, password: String) {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  registerRepository.register(username, firstName,lastName,email, password)
            if (result is Result.Success) {
                _registerResult.value =
                    RegisterResult(success = RegisteredUserView(displayName = result.data.displayName, result.data.email, result.data.username))
            } else {
                Log.i("E!!!!",result.toString())
                if(result.toString()=="Error(exception=java.io.IOException: Error registering: The email address is already in use by another account.)"){
                    _registerResult.value = RegisterResult(error =R.string.register_failed_email)
                }
                else{
                    _registerResult.value = RegisterResult(error = R.string.register_failed)
                }
            }
        }
    }

    fun registerDataChanged(username: String, firstName: String, lastName: String, email: String, password: String) {
        if (!isUserNameValid(username)) {
            _registerForm.value = RegisterFormState(usernameError = R.string.invalid_username)
        }
        if (!isFirstNameValid(firstName)) {
            _registerForm.value = RegisterFormState(firstNameError = R.string.invalid_first_name)
        }
        if (!isLastNameValid(lastName)) {
            _registerForm.value = RegisterFormState(lastNameError = R.string.invalid_last_name)
        }
        if (!isEmailValid(email)) {
            _registerForm.value = RegisterFormState(emailError = R.string.invalid_email)
        }
        if (!isPasswordValid(password)) {
            _registerForm.value = RegisterFormState(passwordError = R.string.invalid_password)
        }
        else {
            _registerForm.value = RegisterFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@") && username.length in 3..20) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank() && username.length in 3..30
        }
    }

    // A placeholder password validation check
    private fun isFirstNameValid(first: String): Boolean {
        return first.length in 3..15
    }

    // A placeholder password validation check
    private fun isLastNameValid(last: String): Boolean {
        return last.length in 3..15
    }

    // A placeholder password validation check
    private fun isEmailValid(email: String): Boolean {
        return (email.contains("@uniandes.edu.co") && email.length in 15..30)
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length in 6..10
    }
}