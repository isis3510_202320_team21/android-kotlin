package com.example.teamup.ui.team

import com.example.teamup.data.model.Team

/**
 * Authentication result : success (user details) or error message.
 */
data class TeamResult(
    val success: Any? = null,
    val error: Int? = null
)