package com.example.teamup.ui.createTeam

import com.google.android.gms.maps.model.LatLng
import java.sql.Time
import java.sql.Date

data class CreateTeamView (
    val name:String,
    val sport:String,
    val level:String,
    val date:String,
    val time:String,
    val latitude:String,
    val longitude:String
)