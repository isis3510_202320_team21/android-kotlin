package com.example.teamup.ui.tournament

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.example.teamup.R
import com.example.teamup.data.Result
import com.example.teamup.data.ResultListTournament
import com.example.teamup.data.TournamentRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TournamentViewModel(private val tournamentRepository: TournamentRepository) : ViewModel() {

    private val _tournamentsResult = MutableLiveData<TournamentResult>()
    val tournamentsResult: LiveData<TournamentResult> = _tournamentsResult

    private val _tournamentsTrackResult = MutableLiveData<TournamentResult>()
    val tournamentsTrackResult: LiveData<TournamentResult> = _tournamentsTrackResult

    fun get() {
        // launch a coroutine in the ViewModel scope
        viewModelScope.launch(Dispatchers.IO) {
            val result = tournamentRepository.getTournaments()
            // switch to the main thread to update UI
            withContext(Dispatchers.Main) {
                if (result is ResultListTournament.Success) {
                    Log.i("6", result.data.toString())
                    _tournamentsResult.value = TournamentResult(success = result.data)
                } else {
                    _tournamentsResult.value = TournamentResult(error = R.string.register_failed)
                }
            }
        }
    }

    fun track(tour: String) {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  tournamentRepository.track(tour)
            if (result is Result.Success) {
                _tournamentsTrackResult.value =
                    TournamentResult(success = TournamentView(result.data.popularTournament))
            } else {
                _tournamentsTrackResult.value = TournamentResult(error = R.string.register_failed)
            }
        }
    }

    fun trackBack(tour: String) {
        // launched asynchronous job
        viewModelScope.launch(Dispatchers.Default) {
            val result =  tournamentRepository.track(tour)
            // switch to the main thread to update UI
            withContext(Dispatchers.Main) {
                if (result is Result.Success) {
                    _tournamentsTrackResult.value =
                        TournamentResult(success = TournamentView(result.data.popularTournament))
                } else {
                    _tournamentsTrackResult.value = TournamentResult(error = R.string.register_failed)
                }
            }
        }
    }

    fun removeTournament(name: String) {
        // launched asynchronous job
        viewModelScope.launch(Dispatchers.IO) {
            val result =  tournamentRepository.removeTournament(name)
        }
    }

}