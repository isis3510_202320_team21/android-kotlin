package com.example.teamup.ui.createTournament

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.teamup.ConnectionLiveData
import com.example.teamup.R
import com.example.teamup.databinding.FragmentCreateTeamBinding
import com.example.teamup.databinding.FragmentCreateTournamentBinding
import com.example.teamup.ui.createTeam.CreateTeamView
import com.example.teamup.ui.createTeam.CreateTeamViewModel
import com.example.teamup.ui.createTeam.CreateTeamViewModelFactory
import java.text.SimpleDateFormat



class CreateTournamentFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentCreateTournamentBinding? = null
    private val binding get() = _binding!!
    private lateinit var createTournamentViewModel: CreateTournamentViewModel
    //private val createTeamViewModel: CreateTeamViewModel by activityViewModels()

    private lateinit var name : String
    private lateinit var sport: String
    private lateinit var date: String
    private lateinit var location:String
    private lateinit var numTeams:String


    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateTournamentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createTournamentViewModel = ViewModelProvider(requireActivity(), CreateTournamentViewModelFactory())
            .get(CreateTournamentViewModel::class.java)
        if (createTournamentViewModel.cache["name"] != "") {
            binding.name.setText(createTournamentViewModel.cache["name"])
        }
        if (createTournamentViewModel.cache["sport"] != "") {
            binding.sport.setText(createTournamentViewModel.cache["sport"])
        }


        if (createTournamentViewModel.cache["date"] != "") {
            val daTe = SimpleDateFormat("dd/MM/yyyy").parse(createTournamentViewModel.cache["date"]).time
            binding.calendar.date = daTe
            println("________________________________________${daTe}______________________________________")
            println(createTournamentViewModel.cache["date"])
            date = createTournamentViewModel.cache["date"]!!
        } else {
            date = ""
        }
        if (createTournamentViewModel.cache["location"] != "") {
            binding.location.setText(createTournamentViewModel.cache["location"])
        }
        if (createTournamentViewModel.cache["numTeams"] != "") {
            binding.numTeams.setText(createTournamentViewModel.cache["numTeams"])
        }

        name = binding.name.text.toString()
        sport = binding.sport.text.toString()
        location = binding.location.text.toString()
        numTeams = binding.numTeams.text.toString()

        createTournamentViewModel.createTournamentFormState.observe(viewLifecycleOwner,
            Observer { createTournamentFormState ->
                if (createTournamentFormState == null) {
                    return@Observer
                }

                createTournamentFormState.sportError?.let {
                    binding.sport.error = getString(it)
                }

                createTournamentFormState.nameError?.let {
                    binding.name.error = getString(it)
                }

                createTournamentFormState.locationError?.let {
                    binding.location.error = getString(it)
                }

                createTournamentFormState.numTeamsError?.let {
                    binding.numTeams.error = getString(it)
                }

            })

        createTournamentViewModel.createTournamentResult.observe(viewLifecycleOwner,
            Observer { createTournamentResult ->
                createTournamentResult ?: return@Observer
                //loadingProgressBar.visibility = View.GONE
                createTournamentResult.error?.let {
                    showCreateTournamentFailed(it)
                }
                createTournamentResult.success?.let {
                    updateUiWithTournament(it)
                }
            })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                name = binding.name.text.toString()
                sport = binding.sport.text.toString()
                location = binding.location.text.toString()
                numTeams = binding.numTeams.text.toString()


                createTournamentViewModel.createTournamentDataChanged(
                    name,
                    sport,
                    date,
                    location,
                    numTeams
                )

                createTournamentViewModel.setTournament(name, sport, date, location, numTeams)

            }


        }
        binding.name.addTextChangedListener(afterTextChangedListener)
        binding.sport.addTextChangedListener(afterTextChangedListener)
        binding.location.addTextChangedListener(afterTextChangedListener)
        binding.numTeams.addTextChangedListener(afterTextChangedListener)
        binding.calendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
        val mes = month+1
        date = "$dayOfMonth/$mes/$year"
            println(binding.calendar.date)
            println(date)
            createTournamentViewModel.createTournamentDataChanged(
                binding.name.text.toString(),
                binding.sport.text.toString(),
                date,
                location,
                numTeams
            )
            createTournamentViewModel.setTournament(name, sport, date, location, numTeams)

        }


        binding.createTournamentBtn.setOnClickListener {
            createTournamentViewModel.setTournament(name, sport, date, location, numTeams)
            //Network checking
            checkNetworkConnection()
            val appContext = context?.applicationContext
            val builder = AlertDialog.Builder(requireActivity())
            //set title for alert dialog
            builder.setTitle(R.string.create_team_label)
            //set message for alert dialog
            builder.setMessage("You are currently offline, check your internet connection to and try again")
            builder.setIcon(R.drawable.baseline_wifi_off_24)
            builder.setPositiveButton("OK") { dialogInterface, which ->

            }
            // Create the AlertDialog
            alertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)

            if (!checkForInternet(appContext!!)) {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            } else {
                createTournamentViewModel.createTournament()
                findNavController().navigate(R.id.TournamentFragment)
            }
            binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
        }
    }

    private fun updateUiWithTournament(model: CreateTournamentView) {
        // TODO : initiate successful logged in experience
        findNavController().navigate(R.id.TournamentFragment)


        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.create_tournament_label)
        //set message for alert dialog
        builder.setIcon(R.drawable.munequito)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun showCreateTournamentFailed(@StringRes errorString: Int) {
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.create_tournament_label)
        //set message for alert dialog
        builder.setMessage(errorString)
        builder.setIcon(android.R.drawable.stat_notify_error)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}