package com.example.teamup.ui.team

import android.graphics.drawable.Drawable
import android.location.Location
import com.example.teamup.data.model.Team
import java.util.*

/**
 * User details post authentication that is exposed to the UI
 */
data class TeamView(
    val teams: MutableList<Team>,
    //... other data fields that may be accessible to the UI
)