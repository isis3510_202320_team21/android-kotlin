package com.example.teamup.ui.user

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.teamup.ConnectionLiveData
import com.example.teamup.R
import com.example.teamup.databinding.MenuLayoutBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*


class MenuFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private var _binding: MenuLayoutBinding? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    val cache:HashMap <String, Calendar?> = hashMapOf("time" to null)

    private lateinit var auth: FirebaseAuth
    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    private var track_pressed:Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        userViewModel = ViewModelProvider(this, UserViewModelFactory())
            .get(UserViewModel::class.java)

        //Network checking
        checkNetworkConnection()
        val appContext = context?.applicationContext
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.team_up_label)
        //set message for alert dialog
        builder.setMessage("You are currently offline. Please beware that some functionalities may require internet connection to function properly.")
        builder.setIcon(R.drawable.baseline_wifi_off_24)
        builder.setPositiveButton("Ok"){dialogInterface , which ->
        }
        // Create the AlertDialog
        alertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)

        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        auth = Firebase.auth
        _binding = MenuLayoutBinding.inflate(inflater, container, false)
        firebaseAnalytics = Firebase.analytics
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!

        binding.trackButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature"){
                param("type","Track")
            }
        }

        binding.sportsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature"){
                param("type","Sports")
            }
            findNavController().navigate(R.id.sportFragment)
            navView.setCheckedItem(R.id.action_Menu_to_sportFragment)
        }
        /*binding.searchButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Search")
            }
        }*/
        binding.teamsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Teams")
            }
            findNavController().navigate(R.id.Teams)
            navView.setCheckedItem(R.id.action_menu_toTeams)
        }
        binding.routinesButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Routines")
            }
//            val builder = AlertDialog.Builder(requireActivity())
//            //set title for alert dialog
//            builder.setTitle(R.string.routines_label)
//            //set message for alert dialog
//            builder.setMessage("This feature will be included soon :)")
//            builder.setIcon(R.drawable.munequito)
//            builder.setPositiveButton("Close"){dialogInterface , which ->
//
//            }
//            // Create the AlertDialog
//            val alertDialog3: AlertDialog = builder.create()
//            // Set other dialog properties
//            alertDialog3.setCancelable(false)
//            alertDialog3.show()
            findNavController().navigate(R.id.action_Menu_to_categoryFragment)
        }
        binding.tournamentsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Tournaments")
            }
            findNavController().navigate(R.id.TournamentFragment)
        }

        binding.weatherButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Weather")
            }
            findNavController().navigate(R.id.Weather)
        }

        val auth = Firebase.auth
        auth.currentUser?.reload()
        //

        val trackButton = binding.trackButton
        val loadingProgressBar = binding.loading

        userViewModel.userResult.observe(viewLifecycleOwner,
            Observer { trackResult ->
                trackResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                trackResult.error?.let {
                    showRegisterFailed(it)
                }
                trackResult.success?.let {
                    updateUiWithUser(it)
                }
            })

        trackButton.setOnClickListener {
            loadingProgressBar.visibility = View.VISIBLE

            val appContext = context?.applicationContext
            track_pressed = true
            if(checkForInternet(appContext!!)){
                userViewModel.track()
                track_pressed = false
                cache["time"]=null
            }
            else{
                cache["time"] = Calendar.getInstance()
                binding.loading.visibility = View.GONE
                //Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                val builder = AlertDialog.Builder(requireActivity())
                //set title for alert dialog
                builder.setTitle(R.string.track)
                //set message for alert dialog
                builder.setMessage("You are currently offline, track of the meeting will be made when connection is restored")
                builder.setIcon(R.drawable.baseline_wifi_off_24)
                builder.setPositiveButton("Ok") { dialogInterface, which ->
                }
                // Create the AlertDialog
                val alertDialog2: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog2.setCancelable(false)
                alertDialog2.show()
            }

        }
    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
                if(track_pressed){
                    if (cache["time"]!=null){
                        userViewModel.trackBack(cache["time"]!!)
                        cache["time"]=null
                        track_pressed = false
                    }
                }
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    private fun updateUiWithUser(model: CurrentUserView) {
        val welcome = model.displayName + " you have met " + model.meetings_7days.toString() + " friend(s) this week!"
        """Toast.makeText(appContext, welcome, Toast.LENGTH_LONG).show()"""

        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.track)
        //set message for alert dialog
        builder.setMessage(welcome)
        builder.setIcon(android.R.drawable.ic_dialog_info)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Close"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog4: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog4.setCancelable(false)
        alertDialog4.setCanceledOnTouchOutside(true);
        alertDialog4.show()

    }
    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}