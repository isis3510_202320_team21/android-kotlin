package com.example.teamup.ui.createTournament

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.data.Result

import com.example.teamup.R
import com.example.teamup.data.CreateTeamRepository
import com.example.teamup.data.CreateTournamentRepository
import com.example.teamup.ui.createTeam.CreateTeamFormState
import com.example.teamup.ui.createTeam.CreateTeamResult
import com.example.teamup.ui.createTeam.CreateTeamView

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreateTournamentViewModel(private val createTournamentRepository: CreateTournamentRepository) : ViewModel() {
    val cache:HashMap <String, String> = hashMapOf("name" to "", "sport" to "", "date" to "", "location" to "", "numTeams" to "")

    private val _createTournamentForm = MutableLiveData<CreateTournamentFormState>()
    val createTournamentFormState: LiveData<CreateTournamentFormState> = _createTournamentForm

    private val _createTournamentResult = MutableLiveData<CreateTournamentResult>()
    val createTournamentResult: LiveData<CreateTournamentResult> = _createTournamentResult

    private val _name = MutableLiveData<String>("")
    val name: LiveData<String> = _name

    private val _sport = MutableLiveData<String> ("")
    val sport: LiveData<String> = _sport

    private val _date = MutableLiveData<String>("")
    val date: LiveData<String> = _date

    private val _location = MutableLiveData<String> ("")
    val location:LiveData<String> = _location

    private val _numTeams = MutableLiveData<String> ("")
    val numTeams:LiveData<String> = _numTeams

    fun setTournament( name:String, sport:String, date:String, location:String, numTeams:String){
        _name.value = name
        cache["name"] = name
        _sport.value = sport
        cache["sport"] = sport
        _date.value = date
        cache["date"] = date
        _location.value = location
        cache["location"]= location
        _numTeams.value = numTeams
        cache["numTeams"] = numTeams
    }



    fun createTournament() {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  createTournamentRepository.createTournament(name.value!!, sport.value!!,  date.value!!, location.value!!, numTeams.value!!)
            if (result is Result.Success) {
                _createTournamentResult.value =
                    CreateTournamentResult(success = CreateTournamentView(result.data.name, result.data.sport, result.data.date, result.data.location, result.data.numTeams))
                cache.remove("name")
                cache.remove("sport")
                cache.remove ("date")
                cache.remove("location")
                cache.remove ("numTeams")
            } else {
                _createTournamentResult.value = CreateTournamentResult(error = R.string.register_failed)
            }
        }
    }

    fun createTournamentDataChanged(name:String, sport:String, level:String, date: String, time: String) {
        if (!isNameValid(name)) {
            _createTournamentForm.value = CreateTournamentFormState(nameError = R.string.invalid_username)
        }
        if (!isSportValid(sport)) {
            _createTournamentForm.value = CreateTournamentFormState(sportError = R.string.invalid_sport)
        }
       /*
        if (!isDateValid(date)) {
            _createTeamForm.value = CreateTeamFormState(dateError = R.string.invalid_date)
        }*/
        /*
        if (!isLocalisationValid(localisation)) {
            _createTeamForm.value = CreateTeamFormState(localisationError = R.string.invalid_localisation)
        }*/
        else {
            _createTournamentForm.value = CreateTournamentFormState(isDataValid = true)
        }
    }

    // A name validation check
    private fun isNameValid(name: String): Boolean {
        return name.length in 3..30
    }

    // A sport validation check
    private fun isSportValid(sport: String): Boolean {
        return sport.length in 3..15
    }

    // A level validation check
    private fun isLevelValid(level: String): Boolean {
        return level.length in 1..15
    }

    // A date validation check
    /*private fun isDateValid(date: Date): Boolean {
        return
    }*/

    // A time validation check
    private fun isTimeValid(time: String): Boolean {
        return (time.length== 5|| time.length==4 && time.substring(0,1).toInt() in 0..23 && time.substring (3,4).toInt() in 0..59) || (time.length == 4 && time.substring(0).toInt() in 0..23 && time.substring(2,3).toInt() in 0..59)
    }
}