package com.example.teamup.ui.team

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.TeamDataSource
import com.example.teamup.data.TeamRepository

/**
 * ViewModel provider factory to instantiate TeamViewModel.
 * Required given TeamViewModel has a non-empty constructor
 */
class TeamViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TeamViewModel::class.java)) {
            return TeamViewModel(
                teamRepository = TeamRepository(
                    dataSource = TeamDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}