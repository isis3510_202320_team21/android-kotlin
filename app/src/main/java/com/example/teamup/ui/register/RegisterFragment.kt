package com.example.teamup.ui.register

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.teamup.ConnectionLiveData
import com.example.teamup.R
import com.example.teamup.databinding.SignUpLayoutBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class RegisterFragment : Fragment() {

    private lateinit var registerViewModel: RegisterViewModel
    private var _binding: SignUpLayoutBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //Network checking
        checkNetworkConnection()
        val appContext = context?.applicationContext
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.register_label)
        //set message for alert dialog
        builder.setMessage("You are currently offline, check your internet connection to continue")
        builder.setIcon(R.drawable.baseline_wifi_off_24)
        builder.setPositiveButton("Go back"){dialogInterface , which ->
            activity?.onBackPressed()
        }
        // Create the AlertDialog
        alertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        if (!checkForInternet(appContext!!)) {
            Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
            alertDialog.show()
        }

        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        auth = Firebase.auth
        _binding = SignUpLayoutBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth.currentUser?.reload()
        registerViewModel = ViewModelProvider(this, RegisterViewModelFactory())
            .get(RegisterViewModel::class.java)

        val usernameEditText = binding.usernameTextEdit
        val passwordEditText = binding.passwordTextEdit
        val firstNameEditText = binding.firstName
        val lastNameEditText = binding.lastName
        val emailEditText = binding.email
        val loginButton = binding.loginButton
        val createAccount = binding.createAccount
        val loadingProgressBar = binding.loading

        registerViewModel.registerFormState.observe(viewLifecycleOwner,
            Observer { registerFormState ->
                if (registerFormState == null) {
                    return@Observer
                }
                createAccount.isEnabled = registerFormState.isDataValid
                registerFormState.usernameError?.let {
                    usernameEditText.error = getString(it)
                }
                registerFormState.firstNameError?.let {
                    firstNameEditText.error = getString(it)
                }
                registerFormState.lastNameError?.let {
                    lastNameEditText.error = getString(it)
                }
                registerFormState.emailError?.let {
                    emailEditText.error = getString(it)
                }
                registerFormState.passwordError?.let {
                    passwordEditText.error = getString(it)
                }
            })

        registerViewModel.registerResult.observe(viewLifecycleOwner,
            Observer { registerResult ->
                registerResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                registerResult.error?.let {
                    showRegisterFailed(it)
                }
                registerResult.success?.let {
                    updateUiWithUser(it)
                }
            })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                registerViewModel.registerDataChanged(
                    usernameEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    emailEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }
        }
        usernameEditText.addTextChangedListener(afterTextChangedListener)
        firstNameEditText.addTextChangedListener(afterTextChangedListener)
        lastNameEditText.addTextChangedListener(afterTextChangedListener)
        emailEditText.addTextChangedListener(afterTextChangedListener)
        passwordEditText.addTextChangedListener(afterTextChangedListener)
        passwordEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registerViewModel.register(
                    usernameEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    emailEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }
            false
        }

        createAccount.setOnClickListener {
            loadingProgressBar.visibility = View.VISIBLE
            registerViewModel.register(
                usernameEditText.text.toString(),
                firstNameEditText.text.toString(),
                lastNameEditText.text.toString(),
                emailEditText.text.toString(),
                passwordEditText.text.toString()
            )
        }
        loginButton.setOnClickListener { findNavController().navigate(R.id.action_Register_to_loginFragment) }
    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner, { isConnected ->
            if (isConnected){
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
            }else{
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        })
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    private fun updateUiWithUser(model: RegisteredUserView) {
        val welcome = getString(R.string.welcome) + model.displayName
        // TODO : initiate successful logged in experience
        findNavController().navigate(R.id.action_Register_to_Menu)

        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
        val textview: TextView = navView.getHeaderView(0).findViewById(R.id.user_name) as TextView
        textview.setText(auth.currentUser?.displayName)

        val textview_email: TextView = navView.getHeaderView(0).findViewById(R.id.user_email) as TextView
        textview_email.setText(auth.currentUser?.email)

        val appContext = context?.applicationContext ?: return

        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.login_label)
        //set message for alert dialog
        builder.setMessage(welcome)
        builder.setIcon(R.drawable.munequito)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("username",model.username)
            putString("email", model.email)
            putString("name", model.displayName)
            apply()
        }
    }

    private fun showRegisterFailed(@StringRes errorString: Int) {
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.register_label)
        //set message for alert dialog
        builder.setMessage(errorString)
        builder.setIcon(android.R.drawable.stat_notify_error)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}