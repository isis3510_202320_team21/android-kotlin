package com.example.teamup.ui.createTournament

data class CreateTournamentFormState (
    val nameError:Int?=null,
    val sportError:Int?=null,
    val dateError: Int?=null,
    val locationError: Int?=null,
    val numTeamsError: Int?=null,
    val isDataValid:Boolean = false
        )
