package com.example.teamup.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.data.LoginRepository
import com.example.teamup.data.Result

import com.example.teamup.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Calendar

class UserViewModel(private val userRepository: LoginRepository) : ViewModel() {

    private val _userResult = MutableLiveData<UserResult>()
    val userResult: LiveData<UserResult> = _userResult

    fun track() {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  userRepository.track()
            if (result is Result.Success) {
                _userResult.value =
                    UserResult(success = CurrentUserView(displayName = result.data.displayName, result.data.email, result.data.meetings_7days.toInt()))
            } else {
                _userResult.value = UserResult(error = R.string.register_failed)
            }
        }
    }

    fun trackBack(time:Calendar) {
        // launched asynchronous job
        viewModelScope.launch(Dispatchers.Default) {
            val result =  userRepository.trackBack(time)
            // switch to the main thread to update UI
            withContext(Dispatchers.Main) {
                if (result is Result.Success) {
                    _userResult.value =
                        UserResult(success = CurrentUserView(displayName = result.data.displayName, result.data.email, result.data.meetings_7days.toInt()))
                } else {
                    _userResult.value = UserResult(error = R.string.register_failed)
                }
            }
        }
    }


}