package com.example.teamup.ui.createTeam

import com.google.android.gms.maps.model.LatLng
import java.sql.Date
import java.sql.Time

data class CreateTeamFormState (
    val nameError:Int?=null,
    val sportError:Int?=null,
    val levelError:Int?=null,
    val dateError: Int?=null,
    val timeError: Int?=null,
    val localisationError: Int?=null,
    val isDataValid:Boolean = false
        )
