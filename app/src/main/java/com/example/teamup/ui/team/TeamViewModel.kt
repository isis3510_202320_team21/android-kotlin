package com.example.teamup.ui.team

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.data.LoginRepository
import com.example.teamup.data.Result

import com.example.teamup.R
import com.example.teamup.data.ResultList
import com.example.teamup.data.TeamRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TeamViewModel(private val teamRepository: TeamRepository) : ViewModel() {

    private val _teamResult = MutableLiveData<TeamResult>()
    val teamResult: LiveData<TeamResult> = _teamResult

    fun track() {
        // launch a coroutine in the ViewModel scope
        viewModelScope.launch(Dispatchers.IO) {
            val result = teamRepository.getTeams()
            // switch to the main thread to update UI
            withContext(Dispatchers.Main) {
                if (result is ResultList.Success) {
                    Log.i("6", result.data.toString())
                    _teamResult.value = TeamResult(success = result.data)
                } else {
                    _teamResult.value = TeamResult(error = R.string.register_failed)
                }
            }
        }
    }

    fun removeTeam(name: String) {
        // launched asynchronous job
        viewModelScope.launch(Dispatchers.IO) {
            val result =  teamRepository.removeTeam(name)
        }
    }

}