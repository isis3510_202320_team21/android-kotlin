package com.example.teamup.ui.register

/**
 * User details post authentication that is exposed to the UI
 */
data class RegisteredUserView(
    val displayName: String,
    val email: String,
    val username: String
    //... other data fields that may be accessible to the UI
)