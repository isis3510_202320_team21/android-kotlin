package com.example.teamup.ui.createTournament

import com.example.teamup.ui.createTeam.CreateTeamView


/**
 * Authentication result : success (user details) or error message.
 */
data class CreateTournamentResult(
    val success: CreateTournamentView? = null,
    val error: Int? = null
)