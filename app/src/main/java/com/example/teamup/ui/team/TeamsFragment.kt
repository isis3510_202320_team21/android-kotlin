package com.example.teamup.ui.team

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.Volley
import com.example.teamup.MyItemRecyclerViewAdapter
import com.example.teamup.R
import com.example.teamup.data.model.Team
import com.example.teamup.databinding.FragmentTeamsListBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


/**
 * A fragment representing a list of Items.
 */
@OptIn(InternalCoroutinesApi::class)
class TeamsFragment : Fragment() {

    private lateinit var teamListenerRegistration: ListenerRegistration

    private var columnCount = 1
    private var _binding: FragmentTeamsListBinding? = null
    private val binding get() = _binding!!

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionId = 2

    private var lon = ""
    private var lat = ""
    private var msg = ""

    private lateinit var teamViewModel: TeamViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTeamsListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getLocation()

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        teamViewModel = ViewModelProvider(this, TeamViewModelFactory())
            .get(TeamViewModel::class.java)
        teamViewModel.track()

        var lista: ArrayList<Team> = ArrayList()

        teamViewModel.teamResult.observe(viewLifecycleOwner,
            Observer { trackResult ->
                trackResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                trackResult.error?.let {
                    showRegisterFailed(it)
                }
                trackResult.success?.let {
                    // Set the adapter
                    lista = it as ArrayList<Team>
                    updateUiWithUser(it as ArrayList<Team>)

                }
            })

        binding.createTeamBtn.setOnClickListener { findNavController().navigate(R.id.CreateTeam)}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }



    }

    private fun updateUiWithUser(model: ArrayList<Team>) {
        val appContext = context?.applicationContext ?: return

        Log.i("SSS", model.size.toString())
        if(model.size<1){

            binding.nolist.visibility = View.VISIBLE
            binding.list.visibility = View.GONE
        }
        else{
            binding.nolist.visibility = View.GONE
            binding.list.visibility = View.VISIBLE
        }

        val recyclerView:RecyclerView = binding.list



        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                var adapter = MyItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : MyItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        var lat_loc = model[position].latitude.toString()
                        var lon_loc = model[position].longitude.toString()

                        Log.i("L", lat_loc +", "+lon_loc)
                        //getWeather(lat_loc,lon_loc)
                        Log.i("TEMP", msg)

                        val builder = AlertDialog.Builder(requireActivity())
                        //set title for alert dialog
                        builder.setTitle(model[position].name.toString())
                        //set message for alert dialog
                        var message = "Remember your "+model[position].sport.toString()+" meeting in "+ model[position].location.toString()+"!"
                        builder.setIcon(R.drawable.munequito)

                        //performing positive action
                        //performing cancel action
                        builder.setPositiveButton("Ok"){dialogInterface , which ->

                        }

                        val queue = Volley.newRequestQueue(requireActivity())
                        val cityName = "Bogota"
                        val url = "https://api.openweathermap.org/data/2.5/weather?lat=$lat_loc&lon=$lon_loc&units=metric&appid=74d1551d47bea8784971fa354ba61b9b"


                        val jsonRequest = JsonObjectRequest(Request.Method.GET,url, null, { response ->
                            val main = response.getJSONObject("main")
                            val temp = main.getString("temp").toFloat()


                            val weather = response.getJSONArray("weather")
                            val weath = weather.getJSONObject(0).getString("main")
                            Log.i("TEMP",temp.toString()+" "+weath)

                            if ((weath == "Clouds" || weath == "Clear") && (temp.toInt() in 13..25)){
                                msg = ""
                            } else {
                                //msg = "You can't do sport outside because the weather is too bad. Do you want to cancel the meeting?"
                                msg = "You can't do sport outside at the meeting location because the weather is too bad. We recommend you to cancel the meeting"
                            }
                            if(msg!="") {//Bad weather

                                message = message + "\n \nALERT!! \n\n" + msg + "\n\nDo you want to cancel it?"
                                builder.setNegativeButton("Delete team") { dialogInterface, which ->
                                    teamViewModel.removeTeam(model[position].name.toString())
                                }
                            }
                            else{
                                message = message + "\n\n There are no problems with the current weather in the meeting location :)"
                            }
                            builder.setMessage(message)
                            val alertDialog: AlertDialog = builder.create()
                            // Set other dialog properties
                            alertDialog.setCancelable(false)
                            alertDialog.setCanceledOnTouchOutside(true);
                            alertDialog.show()
                        }, {
                        })

                        queue.add(jsonRequest)

                    }


                })
            }
        }
    }


   @SuppressLint("MissingPermission", "SetTextI18n")
   @Synchronized
    private fun getLocation() {
        val appContext = context?.applicationContext ?: return
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    val location: Location? = task.result
                    if (location != null) {
                        lon = location.longitude.toString()
                        lat = location.latitude.toString()
                        val geocoder = Geocoder(appContext, Locale.getDefault())
                        val list: List<Address> =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1) as List<Address>
                    }
                }
            } else {
                Toast.makeText(appContext, "Please turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }
    private fun isLocationEnabled(): Boolean {
        val appContext = context?.applicationContext ?: return false
        val locationManager: LocationManager =
            appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }
    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == permissionId) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLocation()
            }
        }
    }

    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TeamsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}