package com.example.teamup.ui.tournament

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.TournamentRepository
import com.example.tournamentup.data.TournamentDataSource

/**
 * ViewModel provider factory to instantiate TournamentViewModel.
 * Required given TournamentViewModel has a non-empty constructor
 */
class TournamentViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TournamentViewModel::class.java)) {
            return TournamentViewModel(
                tournamentRepository = TournamentRepository(
                    dataSource = TournamentDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}