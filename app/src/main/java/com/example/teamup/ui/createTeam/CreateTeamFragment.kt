package com.example.teamup.ui.createTeam

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.teamup.ConnectionLiveData
import com.example.teamup.R
import com.example.teamup.databinding.FragmentCreateTeamBinding
import java.text.SimpleDateFormat


/**
 * A simple [Fragment] subclass.
 * Use the [CreateTeamFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateTeamFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentCreateTeamBinding? = null
    private val binding get() = _binding!!
    private lateinit var createTeamViewModel: CreateTeamViewModel
    //private val createTeamViewModel: CreateTeamViewModel by activityViewModels()

    private lateinit var name : String
    private lateinit var sport: String
    private lateinit var level:String
    private lateinit var time: String
    private lateinit var date: String

    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateTeamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createTeamViewModel = ViewModelProvider(requireActivity(), CreateTeamViewModelFactory())
            .get(CreateTeamViewModel::class.java)
        if (createTeamViewModel.cache["name"] != "") {
            binding.name.setText(createTeamViewModel.cache["name"])
        }
        if (createTeamViewModel.cache["sport"] != "") {
            binding.sport.setText(createTeamViewModel.cache["sport"])
        }
        if (createTeamViewModel.cache["level"] != "") {
            binding.level.setText(createTeamViewModel.cache["level"])
        }
        if (createTeamViewModel.cache["time"] != "") {
            binding.time.setText(createTeamViewModel.cache["time"])
        }
        if (createTeamViewModel.cache["date"] != "") {
            val daTe = SimpleDateFormat("dd/MM/yyyy").parse(createTeamViewModel.cache["date"]).time
            binding.calendar.date = daTe
            println("________________________________________${daTe}______________________________________")
            date = createTeamViewModel.cache["date"]!!
        } else {
            date = ""
        }

        name = binding.name.text.toString()
        sport = binding.sport.text.toString()
        level = binding.level.text.toString()
        println(binding.calendar.date)
        time = binding.time.text.toString()

        createTeamViewModel.createTeamFormState.observe(viewLifecycleOwner,
            Observer { createTeamFormState ->
                if (createTeamFormState == null) {
                    return@Observer
                }

                createTeamFormState.sportError?.let {
                    binding.sport.error = getString(it)
                }

                createTeamFormState.levelError?.let {
                    binding.level.error = getString(it)
                }
            })

        createTeamViewModel.createTeamResult.observe(viewLifecycleOwner,
            Observer { createTeamResult ->
                createTeamResult ?: return@Observer
                //loadingProgressBar.visibility = View.GONE
                createTeamResult.error?.let {
                    showCreateTeamFailed(it)
                }
                createTeamResult.success?.let {
                    updateUiWithTeam(it)
                }
            })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                name = binding.name.text.toString()
                sport = binding.sport.text.toString()
                level = binding.level.text.toString()
                time = binding.time.text.toString()


                createTeamViewModel.createTeamDataChanged(
                    name,
                    sport,
                    level,
                    date,
                    time
                )
            }


        }
        binding.name.addTextChangedListener(afterTextChangedListener)
        binding.sport.addTextChangedListener(afterTextChangedListener)
        binding.level.addTextChangedListener(afterTextChangedListener)
        binding.time.addTextChangedListener(afterTextChangedListener)
        binding.calendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val mes = month +1
            date = "$dayOfMonth/$mes/$year"
            println(binding.calendar.date)
            println(date)
            createTeamViewModel.createTeamDataChanged(
                binding.name.text.toString(),
                binding.sport.text.toString(),
                binding.level.text.toString(),
                date,
                binding.time.text.toString(),
            )
        }


        binding.locationBtn.setOnClickListener {
            createTeamViewModel.setTeam(name, sport, level, date, time)
            //Network checking
            checkNetworkConnection()
            val appContext = context?.applicationContext
            val builder = AlertDialog.Builder(requireActivity())
            //set title for alert dialog
            builder.setTitle(R.string.create_team_label)
            //set message for alert dialog
            builder.setMessage("You are currently offline, check your internet connection to and try again")
            builder.setIcon(R.drawable.baseline_wifi_off_24)
            builder.setPositiveButton("OK") { dialogInterface, which ->

            }
            // Create the AlertDialog
            alertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)

            if (!checkForInternet(appContext!!)) {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            } else {
                findNavController().navigate(R.id.action_createTeam_toSearchStages)
            }
            binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
        }
    }

    private fun updateUiWithTeam(model: CreateTeamView) {
        // TODO : initiate successful logged in experience
        findNavController().navigate(R.id.action_createTeam_toTeams)


        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.create_team_label)
        //set message for alert dialog
        builder.setIcon(R.drawable.munequito)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun showCreateTeamFailed(@StringRes errorString: Int) {
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.create_team_label)
        //set message for alert dialog
        builder.setMessage(errorString)
        builder.setIcon(android.R.drawable.stat_notify_error)

        //performing positive action
        //performing cancel action
        builder.setPositiveButton("Ok"){dialogInterface , which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}