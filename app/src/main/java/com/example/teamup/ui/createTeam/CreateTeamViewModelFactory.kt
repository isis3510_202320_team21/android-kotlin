package com.example.teamup.ui.createTeam

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.CreateTeamDataSource
import com.example.teamup.data.CreateTeamRepository
import com.example.teamup.data.LoginDataSource
import com.example.teamup.data.LoginRepository
import com.example.teamup.data.model.CreateTeam
import com.example.teamup.ui.register.RegisterViewModel

/**
 * ViewModel provider factory to instantiate RegisterViewModel.
 * Required given RegisterViewModel has a non-empty constructor
 */
class CreateTeamViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateTeamViewModel::class.java)) {
            return CreateTeamViewModel(
                createTeamRepository = CreateTeamRepository(
                    dataSource = CreateTeamDataSource()
                )
            ) as T

        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}