package com.example.teamup.ui.createTeam

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.data.Result

import com.example.teamup.R
import com.example.teamup.data.CreateTeamRepository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreateTeamViewModel(private val createTeamRepository: CreateTeamRepository) : ViewModel() {
    val cache:HashMap <String, String> = hashMapOf("name" to "", "sport" to "", "level" to "", "date" to "", "time" to "", "latitude" to "", "longitude" to "")

    private val _createTeamForm = MutableLiveData<CreateTeamFormState>()
    val createTeamFormState: LiveData<CreateTeamFormState> = _createTeamForm

    private val _createTeamResult = MutableLiveData<CreateTeamResult>()
    val createTeamResult: LiveData<CreateTeamResult> = _createTeamResult

    private val _name = MutableLiveData<String>("")
    val name: LiveData<String> = _name

    private val _sport = MutableLiveData<String> ("")
    val sport: LiveData<String> = _sport

    private val _level = MutableLiveData<String>("")
    val level: LiveData<String> = _level

    private val _date = MutableLiveData<String>("")
    val date: LiveData<String> = _date

    private val _time = MutableLiveData<String>("")
    val time: LiveData<String> = _time

    private val _latitude = MutableLiveData<String>("")
    val latitude: LiveData<String> = _latitude

    private val _longitude = MutableLiveData<String>("")
    val longitude: LiveData<String> = _longitude


    fun setTeam( name:String, sport:String, level:String, date:String, time:String){
        _name.value = name
        cache["name"] = name
        _sport.value = sport
        cache["sport"] = sport
        _level.value = level
        cache["level"] = level
        _date.value = date
        cache["date"] = date
        _time.value = time
        cache["time"] = time
    }

    fun setTeamLatLng (latitude:String, longitude: String){
        _latitude.value = latitude
        cache["latitude"] = latitude
        _longitude.value = longitude
        cache["longitude"]= longitude
    }

    fun createTeam() {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  createTeamRepository.createTeam(name.value!!, sport.value!!, level.value!!, date.value!!, time.value!!, latitude.value!!, longitude.value!!)
            if (result is Result.Success) {
                _createTeamResult.value =
                    CreateTeamResult(success = CreateTeamView(result.data.name, result.data.sport, result.data.level, result.data.date, result.data.time, result.data.latitude, result.data.longitude))
                cache.remove("name")
                cache.remove("sport")
                cache.remove ("level")
                cache.remove ("date")
                cache.remove ("time")
                cache.remove ("latitude")
                cache.remove ("longitude")
            } else {
                _createTeamResult.value = CreateTeamResult(error = R.string.register_failed)
            }
        }
    }

    fun createTeamDataChanged(name:String, sport:String, level:String, date: String, time: String) {
        if (!isNameValid(name)) {
            _createTeamForm.value = CreateTeamFormState(nameError = R.string.invalid_username)
        }
        if (!isSportValid(sport)) {
            _createTeamForm.value = CreateTeamFormState(sportError = R.string.invalid_sport)
        }
        if (!isLevelValid(level)) {
            _createTeamForm.value = CreateTeamFormState(levelError = R.string.invalid_level)
        }/*
        if (!isDateValid(date)) {
            _createTeamForm.value = CreateTeamFormState(dateError = R.string.invalid_date)
        }*/
        if (!isTimeValid(time)) {
            _createTeamForm.value = CreateTeamFormState(timeError = R.string.invalid_time)
        }/*
        if (!isLocalisationValid(localisation)) {
            _createTeamForm.value = CreateTeamFormState(localisationError = R.string.invalid_localisation)
        }*/
        else {
            _createTeamForm.value = CreateTeamFormState(isDataValid = true)
        }
    }

    // A name validation check
    private fun isNameValid(name: String): Boolean {
        return name.length in 3..20
    }

    // A sport validation check
    private fun isSportValid(sport: String): Boolean {
        return sport.length in 3..15
    }

    // A level validation check
    private fun isLevelValid(level: String): Boolean {
        return level.length in 1..15
    }

    // A date validation check
    /*private fun isDateValid(date: Date): Boolean {
        return
    }*/

    // A time validation check
    private fun isTimeValid(time: String): Boolean {
        return (time.length== 5|| time.length==4 && time.substring(0,1).toInt() in 0..23 && time.substring (3,4).toInt() in 0..59) || (time.length == 4 && time.substring(0).toInt() in 0..23 && time.substring(2,3).toInt() in 0..59)
    }
}