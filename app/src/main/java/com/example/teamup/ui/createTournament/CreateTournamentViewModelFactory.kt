package com.example.teamup.ui.createTournament

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.CreateTeamDataSource
import com.example.teamup.data.CreateTeamRepository
import com.example.teamup.data.CreateTournamentDataSource
import com.example.teamup.data.CreateTournamentRepository
import com.example.teamup.ui.createTeam.CreateTeamViewModel

/**
 * ViewModel provider factory to instantiate RegisterViewModel.
 * Required given RegisterViewModel has a non-empty constructor
 */
class CreateTournamentViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateTournamentViewModel::class.java)) {
            return CreateTournamentViewModel(
                createTournamentRepository = CreateTournamentRepository(
                    dataSource = CreateTournamentDataSource()
                )
            ) as T

        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}