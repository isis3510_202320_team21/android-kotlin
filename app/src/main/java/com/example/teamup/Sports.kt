package com.example.teamup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.SportsLayoutBinding

// TODO: Rename parameter arguments, choose names that match



/**
 * A simple [Fragment] subclass.
 * Use the [Login.newInstance] factory method to
 * create an instance of this fragment.
 */
class Sports : Fragment() {


    private var _binding: SportsLayoutBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = SportsLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.action_Sports_to_Menu) }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
