package com.example.teamup

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.teamup.data.model.Team

import com.example.teamup.databinding.TeamLayoutBinding

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class MyItemRecyclerViewAdapter(
    private val values: List<Team>
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)
        
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            TeamLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.name_text.text = item.name
        holder.date_text.text = item.date.toString()
        if (item.sport =="Basketball") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_basketball_24)
        }
        else if (item.sport =="Soccer") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_soccer_24)
        }
        else if (item.name =="Tennis") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_tennis_24)
        }
        else if (item.name =="Volley") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_volleyball_24)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: TeamLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {
        val name_text: TextView = binding.teamName
        val date_text: TextView = binding.dateText
        val team_image: ImageView = binding.teamImage

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name_text.text + "'"
        }
    }

}