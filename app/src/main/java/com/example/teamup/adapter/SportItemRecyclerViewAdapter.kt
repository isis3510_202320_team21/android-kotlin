package com.example.teamup

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.teamup.data.model.Team
import com.example.teamup.databinding.SportLayoutBinding

import com.example.teamup.databinding.TeamLayoutBinding
import com.example.teamup.sport.Sport

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class SportItemRecyclerViewAdapter(
    private val values: List<Sport>
) : RecyclerView.Adapter<SportItemRecyclerViewAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)
        
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            SportLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.name_text.text = item.name
        holder.add_text.text = item.description
        if (item.name =="Basketball") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_basketball_24)
        }
        else if (item.name =="Soccer") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_soccer_24)
        }
        else if (item.name =="Tennis") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_tennis_24)
        }
        else if (item.name =="Volley") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_volleyball_24)
        }
        else if(item.name == "American Football"){
            Glide.with(holder.itemView.context)
                .load(item.imageURL.toString())
                .into(holder.team_image)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: SportLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {
        val name_text: TextView = binding.sportName
        val add_text: TextView = binding.additionalText
        val team_image: ImageView = binding.sportImage

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name_text.text + "'"
        }
    }

}