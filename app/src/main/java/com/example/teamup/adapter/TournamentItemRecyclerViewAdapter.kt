package com.example.teamup

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.teamup.data.model.Tournament

import com.example.teamup.databinding.TournamentLayoutBinding

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class TournamentItemRecyclerViewAdapter(
    private val values: List<Tournament>
) : RecyclerView.Adapter<TournamentItemRecyclerViewAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)
        
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            TournamentLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.name_text.text = item.name
        holder.sport_text.text = item.sport.toString()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: TournamentLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {
        val name_text: TextView = binding.tournamentName
        val sport_text: TextView = binding.sportText

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name_text.text + "'"
        }
    }

}