package com.example.teamup.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.teamup.databinding.SportLayoutBinding
import com.example.teamup.sport.Sport
import com.bumptech.glide.Glide


class SportListAdapater(var context: Context, var list: ArrayList<Sport>) : RecyclerView.Adapter<SportListAdapater.ViewHolder>(){

    class ViewHolder(itemView: SportLayoutBinding):RecyclerView.ViewHolder(itemView.root){

        var name_text = itemView.sportName
        var add_text = itemView.additionalText
        var sport_image = itemView.sportImage
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            SportLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name_text.text = list[position].name
        holder.add_text.text = list[position].description
        Glide.with(holder.itemView.context)
            .load(list[position].imageURL.toString())
            .into(holder.sport_image)
//        holder.sport_image.setImage(list[position].image)

//        holder.itemView.setOnClickListener{
//            if (mListener != null){
//                mListener.onItemClick(position)
//            }
//        }
    }
}