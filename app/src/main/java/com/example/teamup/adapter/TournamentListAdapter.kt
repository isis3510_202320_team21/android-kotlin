package com.example.teamup.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.data.model.Tournament
import com.example.teamup.databinding.TournamentLayoutBinding

class TournamentListAdapter(var context: Context, var list: ArrayList<Tournament>):RecyclerView.Adapter<TournamentListAdapter.ViewHolder>() {
    class ViewHolder(itemView: TournamentLayoutBinding):RecyclerView.ViewHolder(itemView.root){

        var name_text = itemView.tournamentName
        var sport_text = itemView.sportText
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            TournamentLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
    return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name_text.text = list[position].name
        holder.sport_text.text = list[position].sport.toString()
    }
}
