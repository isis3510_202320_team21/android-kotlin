package com.example.teamup.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.R
import com.example.teamup.data.model.Team
import com.example.teamup.databinding.TeamLayoutBinding

class TeamListAdapter(var context: Context, var list: ArrayList<Team>):RecyclerView.Adapter<TeamListAdapter.ViewHolder>() {
    class ViewHolder(itemView: TeamLayoutBinding):RecyclerView.ViewHolder(itemView.root){

        var name_text = itemView.teamName
        var date_text = itemView.dateText
        var team_image = itemView.teamImage
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            TeamLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
    return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name_text.text = list[position].name
        holder.date_text.text = list[position].date.toString()
        if (list[position].sport =="Basketball") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_basketball_24)
        }
    }
}
