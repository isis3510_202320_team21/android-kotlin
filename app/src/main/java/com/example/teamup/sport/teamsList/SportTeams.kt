package com.example.teamup.sport.teamsList

data class SportTeams(
    val name: String?=null,
    val date: String?=null,
    val location: String?=null,
    val level: String?=null,
    val sport:String?=null,
    val latitude: Double?=null,
    val longitude:Double?=null,
    //val sportIm: Drawable,
)
