package com.example.teamup.sport

import android.graphics.drawable.Drawable
import android.location.Location
import java.util.Date

data class Sport(
    val id: Int?=0,
    val name: String?="",
    val description: String?="",
//    val image: Int
    val imageURL: String?="")
//    val imageUrl: String )
