package com.example.teamup.sport.teamsList

import android.util.Log
import com.example.teamup.data.SportResultList
import com.example.teamup.sport.teamsList.SportTeams
import com.example.teamup.sport.teamsList.SportTeamsServiceAdapter

class SportTeamsRepository (val dataSource: SportTeamsServiceAdapter){
    suspend fun getTeams(): SportTeamsResultList<MutableList<SportTeams>> {
        val result = dataSource.getTeams()

        if (result is SportTeamsResultList.Success) {
            if (result.data?.isNotEmpty() == true) {
                result.data[0].name?.let { Log.i("3", it) }
            }
            return result
            // append to the list???
        } else {
            return SportTeamsResultList.Error(Exception("Failed to get sports"))
        }
    }
}