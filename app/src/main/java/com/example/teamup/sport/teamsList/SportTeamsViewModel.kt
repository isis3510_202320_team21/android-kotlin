package com.example.teamup.sport.teamsList

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.example.teamup.sport.teamsList.SportTeamsRepository
import com.example.teamup.R
import kotlinx.coroutines.withContext

class SportTeamsViewModel(val sportTeamsRepository: SportTeamsRepository) : ViewModel() {
    // TODO: Implement the ViewModel

    private var _teamsResult = MutableLiveData<SportTeamsResult>()
    val teamsResult: LiveData<SportTeamsResult> = _teamsResult

    private val sportTeamsAppService = SportTeamsAppService()

    //can implement as sparse array i think
    val cacheSearchResults: MutableLiveData<List<SportTeams>> by lazy {
        MutableLiveData<List<SportTeams>>()
    }

    fun searchTeams(query: String){
        val results = sportTeamsAppService.searchTeams(query)
        cacheSearchResults.value = results
    }

    fun filterTeams(query: String){
        val results = sportTeamsAppService.filterTeamsBySport(query)
        cacheSearchResults.postValue(results)
    }

    fun track(sportName: String) {
        // launch a coroutine in the ViewModel scope
        viewModelScope.launch(Dispatchers.IO) {
            val result = sportTeamsRepository.getTeams()
            // switch to Default dispatcher to perform sorting
            val sortedResult = withContext(Dispatchers.Default) {
                when (result) {
                    is SportTeamsResultList.Success -> {
                        result.data?.let {
                            sportTeamsAppService.setTeams(it) // First, set the teams in your app service
                            filterTeams(sportName) // Replace "the sport name" with the actual sport name
                        }
                        SportTeamsResultList.Success(cacheSearchResults.value as MutableList<SportTeams>?)
                    }
                    else -> result
                }
            }

            // switch to the main thread to update UI
            withContext(Dispatchers.Main) {
                when (sortedResult) {
                    is SportTeamsResultList.Success -> {
                        Log.i("6", sortedResult.data.toString())
                        _teamsResult.value = SportTeamsResult(success = sortedResult.data)
                        sortedResult.data?.let { sportTeamsAppService.setTeams(it) }
                    }
                    else -> {
                        _teamsResult.value = SportTeamsResult(error = R.string.register_failed)
                    }
                }
            }
        }
    }


    fun track1(){
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  sportTeamsRepository.getTeams()
            if (result is SportTeamsResultList.Success) {
                Log.i("6", result.data.toString())
                _teamsResult.value = SportTeamsResult(success = result.data)
                result.data?.let { sportTeamsAppService.setTeams(it) }
            } else {
                _teamsResult.value = SportTeamsResult(error = R.string.register_failed)
            }
        }
    }

}