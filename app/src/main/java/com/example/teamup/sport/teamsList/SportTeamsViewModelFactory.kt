package com.example.teamup.sport.teamsList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SportTeamsViewModelFactory: ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SportTeamsViewModel::class.java)) {
            return SportTeamsViewModel(
                sportTeamsRepository = SportTeamsRepository(
                    dataSource = SportTeamsServiceAdapter()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
