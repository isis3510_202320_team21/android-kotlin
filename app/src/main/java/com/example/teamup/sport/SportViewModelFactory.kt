package com.example.teamup.sport


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.TeamDataSource
import com.example.teamup.data.TeamRepository

class SportViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SportViewModel::class.java)) {
            return SportViewModel(
                sportRepository = SportRepository(
                    dataSource = SportServiceAdapter()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}