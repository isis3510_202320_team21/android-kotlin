package com.example.teamup.sport

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.SportResultList
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.ArrayList

class SportServiceAdapter {
    private var auth: FirebaseAuth = Firebase.auth
    var user: FirebaseUser? = null
    private val db = Firebase.firestore
    private val sportsList: MutableList<Sport> = ArrayList()

    suspend fun getSports(): SportResultList<MutableList<Sport>> {
        var msg:String = ""
        val email = auth.currentUser?.email.toString()
        val dbRef = db.collection("sports")

        sportsList.clear()
//        dbRef.get().addOnSuccessListener {
//            if (!it.isEmpty){
//                for (data in it.documents){
//                    val sport: Sport? = data.toObject(Sport::class.java)
//                    if (sport != null){
//                        sport.name?.let { it1 -> Log.i("1", it1) }
//                        sportsList.add(sport)
//                        Log.i("1", sportsList.size.toString())
//
//                    }
//                }
//            }
//        }.addOnFailureListener{exception ->
//            Log.w(ContentValues.TAG, "Error getting SPORT documents: ", exception)
//            msg = exception.message.toString()
//        }.await()
        try {
            val snapshot = dbRef.get().await() // await the result of the Firestore operation

            for (data in snapshot.documents) {
                val sport: Sport? = data.toObject(Sport::class.java)
                if (sport != null) {
                    sport.name?.let { it1 -> Log.i("1", it1) }
                    sportsList.add(sport)
                    Log.i("1", sportsList.size.toString())
                }
            }

            return SportResultList.Success(sportsList)

        } catch (exception: Exception) {
            Log.w(ContentValues.TAG, "Error getting SPORT documents: ", exception)
            msg = exception.message.toString()
            return SportResultList.Error(Exception("Error logging in$msg"))
        }

//        return if (msg == ""){
//            sportsList[0].name?.let { Log.i("3", it) }
//            SportResultList.Success(sportsList)
//        } else {
//            SportResultList.Error(Exception("Error logging in$msg"))
//        }

    }
}