package com.example.teamup.sport.teamsList

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.SportResultList
import com.example.teamup.sport.Sport
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.ArrayList

class SportTeamsServiceAdapter {
    private var auth: FirebaseAuth = Firebase.auth
    var user: FirebaseUser? = null
    private val db = Firebase.firestore
    private val sportTeamsList: MutableList<SportTeams> = ArrayList()

    suspend fun getTeams(): SportTeamsResultList<MutableList<SportTeams>> {
        var msg:String = ""
        val email = auth.currentUser?.email.toString()
        val dbRef = db.collection("teams-list")
        sportTeamsList.clear()
        try{
            val snapshot = dbRef.get().await()
            for (data in snapshot.documents){
                val team: SportTeams? = data.toObject(SportTeams::class.java)
                if (team != null){
                    team.name?.let { it1 -> Log.i("1", it1) }
                    sportTeamsList.add(team)
                    //Log.i("1", "tamanio de la lista"+sportTeamsList.size.toString())
                }
            }

            return SportTeamsResultList.Success(sportTeamsList)

        } catch (exception: Exception){
            Log.w(ContentValues.TAG, "Error getting TEAMS documents: ", exception)
            msg = exception.message.toString()
            return SportTeamsResultList.Error(Exception("Error logging in$msg"))
        }
//        dbRef.get().addOnSuccessListener {
//            if (!it.isEmpty){
//                for (data in it.documents){
//                    val team: SportTeams? = data.toObject(SportTeams::class.java)
//                    if (team != null){
//                        team.name?.let { it1 -> Log.i("1", it1) }
//                        sportTeamsList.add(team)
//                        Log.i("1", "tamanio de la lista"+sportTeamsList.size.toString())
//
//                    }
//                }
//            }
//        }.addOnFailureListener{exception ->
//            Log.w(ContentValues.TAG, "Error getting SPORT documents: ", exception)
//            msg = exception.message.toString()
//        }.await()
//
//        return if (msg == ""){
//            if (sportTeamsList.isNotEmpty()) {
//                sportTeamsList[0].name?.let { Log.i("3", it) }
//            }
//            SportTeamsResultList.Success(sportTeamsList)
//        } else {
//            SportTeamsResultList.Error(Exception("Error logging in$msg"))
//        }
    }
}