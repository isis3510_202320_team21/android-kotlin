package com.example.teamup.sport

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.teamup.MyItemRecyclerViewAdapter
import com.example.teamup.R
import com.example.teamup.SportItemRecyclerViewAdapter
import com.example.teamup.data.model.Team
import com.example.teamup.databinding.FragmentSportBinding
import com.example.teamup.ui.team.TeamViewModel
import com.example.teamup.ui.team.TeamViewModelFactory
import com.example.teamup.ui.team.TeamsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.ktx.Firebase
import java.util.*

class SportFragment : Fragment() {

    private lateinit var sportListenerRegistration: ListenerRegistration

    private var columnCount = 1
    private var _binding: FragmentSportBinding? = null
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics


    private lateinit var mFusedLocationClient : FusedLocationProviderClient
    private val permissionId = 2

    private var lon = ""
    private var lat = ""
    private var msg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSportBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
//        getLocation()

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        val sportViewModel = ViewModelProvider(this, SportViewModelFactory())
            .get(SportViewModel::class.java)
        sportViewModel.track()


        sportViewModel.sportResult.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result.error?.let {
                showRegisterFailed(it)
            }
            result.success?.let {
                updateUiWithUser(it as ArrayList<Sport>)
            }
        }

        sportViewModel.cacheSearchResults.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result?.let {
                updateUiWithUser(it as ArrayList<Sport>)
            }
        }

        binding.searchSportBtn.setOnClickListener {
            val query = binding.inputSearchSport.text.toString()
            sportViewModel.searchSport(query)
        }
        // mirar la creacion de acciones en el xml
        //binding.c
        // reateTeamBtn.setOnClickListener { teamViewModel.track()}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }



    }

    private fun updateUiWithUser(model: ArrayList<Sport>) {
        val appContext = context?.applicationContext ?: return

        val recyclerView: RecyclerView = binding.list

        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                var adapter = SportItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : SportItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        //BQ deporte mas popular
                        firebaseAnalytics.logEvent("sports"){
                            param("sport", model[position].name.toString())
                        }

//                        var lat_loc = model[position].latitude.toString()
//                        var lon_loc = model[position].longitude.toString()

//                        Log.i("L", lat_loc +", "+lon_loc)
                        //getWeather(lat_loc,lon_loc)
                        Log.i("TEMP", msg)

                        val builder = AlertDialog.Builder(requireActivity())
                        //set title for alert dialog
                        builder.setTitle(model[position].name)
                        //set message for alert dialog
                        var message = "You just tapped on "+ model[position].name +"!"
                        builder.setIcon(R.drawable.munequito)

                        //performing positive action
                        //performing cancel action
                        builder.setPositiveButton("Ok"){dialogInterface , which ->

                        }

//                        val queue = Volley.newRequestQueue(requireActivity())
//                        val cityName = "Bogota"
//                        val url = "https://api.openweathermap.org/data/2.5/weather?lat=$lat_loc&lon=$lon_loc&units=metric&appid=74d1551d47bea8784971fa354ba61b9b"
//
//
//                        val jsonRequest = JsonObjectRequest(Request.Method.GET,url, null, { response ->
//                            val main = response.getJSONObject("main")
//                            val temp = main.getString("temp").toFloat()
//
//
//                            val weather = response.getJSONArray("weather")
//                            val weath = weather.getJSONObject(0).getString("main")
//                            Log.i("TEMP",temp.toString()+" "+weath)
//
//                            if ((weath == "Clouds" || weath == "Clear") && (temp.toInt() in 13..25)){
//                                msg = ""
//                            } else {
//                                //msg = "You can't do sport outside because the weather is too bad. Do you want to cancel the meeting?"
//                                msg = "You can't do sport outside at the meeting location because the weather is too bad. We recommend you to cancel the meeting"
//                            }
//                            if(msg!="") {//Bad weather
//
//                                message = message + "\n \nALERT!! \n\n" + msg
//                                /*builder.setNegativeButton("Delete team") { dialogInterface, which ->
//
//                                }*/
//                                builder.setMessage(message)
//                                val alertDialog: AlertDialog = builder.create()
//                                // Set other dialog properties
//                                alertDialog.setCancelable(false)
//                                alertDialog.show()
//                            }
//                        }, {





                        //queue.add(jsonRequest

                        //findNavController().navigate(R.id.sportTeamsFragment)
                        val sportName = model[position].name ?: ""
                        val action = SportFragmentDirections.actionSportFragmentToSportTeamsFragment(sportName)

                        findNavController().navigate(action)


//                        NavigationView.(R.id.action_sportFragment_to_sportTeamsFragment)
                    }
                })
            }
        }
    }


//    @SuppressLint("MissingPermission", "SetTextI18n")
//    @Synchronized
//    private fun getLocation() {
//        val appContext = context?.applicationContext ?: return
//        if (checkPermissions()) {
//            if (isLocationEnabled()) {
//                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
//                    val location: Location? = task.result
//                    if (location != null) {
//                        lon = location.longitude.toString()
//                        lat = location.latitude.toString()
//                        val geocoder = Geocoder(appContext, Locale.getDefault())
//                        val list: List<Address> =
//                            geocoder.getFromLocation(location.latitude, location.longitude, 1) as List<Address>
//                    }
//                }
//            } else {
//                Toast.makeText(appContext, "Please turn on location", Toast.LENGTH_LONG).show()
//                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
//                startActivity(intent)
//            }
//        } else {
//            requestPermissions()
//        }
//    }
//    private fun isLocationEnabled(): Boolean {
//        val appContext = context?.applicationContext ?: return false
//        val locationManager: LocationManager =
//            appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
//            LocationManager.NETWORK_PROVIDER
//        )
//    }
    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }

//    @SuppressLint("MissingSuperCall")
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String>,
//        grantResults: IntArray
//    ) {
//        if (requestCode == permissionId) {
//            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                getLocation()
//            }
//        }
//    }

    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TeamsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

}

