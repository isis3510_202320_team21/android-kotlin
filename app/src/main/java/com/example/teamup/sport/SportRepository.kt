package com.example.teamup.sport;
import android.util.Log
import com.example.teamup.data.SportResultList

public class SportRepository (val dataSource: SportServiceAdapter) {
    suspend fun getSports():SportResultList<MutableList<Sport>>{
        val result = dataSource.getSports()

        if (result is SportResultList.Success) {
            Log.i("4", result.data?.get(0)?.name.toString())
            return result
            // append to the list???
        } else {
            return SportResultList.Error(Exception("Failed to get sports"))
        }
    }
}
