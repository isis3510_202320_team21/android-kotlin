package com.example.teamup.sport.teamsList

data class SportTeamsResult(
    val success: Any? = null,
    val error: Int? = null
)
