package com.example.teamup.sport.teamsList

import android.util.Log

class SportTeamsAppService {
    private var teamsList: List<SportTeams> = listOf()

    @Synchronized
    fun setTeams(teams: List<SportTeams>) {
        teamsList = teams.toList() // Make a defensive copy of the list
    }

    fun searchTeams(query: String): List<SportTeams> {
        return teamsList.filter { team -> team.name?.contains(query, ignoreCase=true) ?: false }
    }

    fun filterTeamsBySport(query: String): List<SportTeams> {
        val filtered = teamsList.filter { team -> team.sport?.contains(query, ignoreCase=true) ?: false }
        Log.i("filtered", filtered.toString())
        return filtered
    }
}