package com.example.teamup.sport.teamsList

data class SportTeamsView(
    val sportTeams: MutableList<SportTeams>
)
