package com.example.teamup.sport

data class SportResult(
    val success: Any? = null,
    val error: Int? = null
)