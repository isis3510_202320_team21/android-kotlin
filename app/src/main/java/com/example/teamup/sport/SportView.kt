package com.example.teamup.sport

data class SportView(
  val sports: MutableList<Sport>
)