package com.example.teamup.sport.teamsList

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.R
import com.example.teamup.SportItemRecyclerViewAdapter
import com.example.teamup.databinding.TeamsLayoutBinding
import com.example.teamup.sport.teamsList.SportTeams

class SportTeamsItemRecyclerViewAdapter(private val values: List<SportTeams>) :
    RecyclerView.Adapter<SportTeamsItemRecyclerViewAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            TeamsLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.name_text.text = item.name
        holder.location_text.text= item.location
        holder.date_text.text = item.date

        if (item.name =="Basketball") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_basketball_24)
        }
        else if (item.name =="Soccer") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_soccer_24)
        }
        else if (item.name =="Tennis") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_tennis_24)
        }
        else if (item.name =="Volley") {
            holder.team_image.setImageResource(R.drawable.baseline_sports_volleyball_24)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: TeamsLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {
        val name_text: TextView = binding.teamName
        val location_text: TextView = binding.locationText
        val date_text: TextView = binding.dateText
        val team_image: ImageView = binding.sportImage

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name_text.text + "'"
        }
    }

}