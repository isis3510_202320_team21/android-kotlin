package com.example.teamup.sport.teamsList

import com.example.teamup.sport.teamsList.SportTeams


sealed class SportTeamsResultList<out T :Any> {
    data class Success<out T : Any>(val data: MutableList<SportTeams>?) : SportTeamsResultList<T>()
    data class Error(val exception: Exception) : SportTeamsResultList<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}
