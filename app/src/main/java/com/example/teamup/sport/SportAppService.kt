package com.example.teamup.sport

class SportAppService {
    //list to store all sports
    private var sportsList:MutableList<Sport> = mutableListOf()

    //function to add sport to the list
    fun addSport(sport: Sport){
        sportsList.add(sport)
    }

    fun setSports(sports: MutableList<Sport>){
        sportsList = sports
    }

    // get all the sports
    fun getAllSports():List<Sport>{
        return sportsList
    }

    // Function to search for sports based on a query (if the list grows too big, we can use a better search algorithm, like binary search)
    fun searchSports(query:String) : List<Sport>{
        return sportsList.filter { sport -> sport.name?.contains(query, ignoreCase=true) ?: false }
    }


}