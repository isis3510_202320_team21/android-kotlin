package com.example.teamup.sport.teamsList

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
//SportTeamsLayoutBinding
import com.example.teamup.databinding.TeamsLayoutBinding
import com.example.teamup.sport.teamsList.SportTeams
import com.bumptech.glide.Glide


class SportTeamsAdapter(var context: Context, var list: ArrayList<SportTeams>) : RecyclerView.Adapter<SportTeamsAdapter.ViewHolder>() {

        class ViewHolder(itemView: TeamsLayoutBinding):RecyclerView.ViewHolder(itemView.root){

            var name_text = itemView.teamName
            var location_text = itemView.locationText
            var date_text = itemView.dateText
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            return ViewHolder(
                TeamsLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            // to change this

            holder.name_text.text = list[position].name
            holder.location_text.text = list[position].location
            holder.date_text.text = list[position].date

    }
}