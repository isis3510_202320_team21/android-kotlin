package com.example.teamup.sport

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.teamup.R
import com.example.teamup.data.SportResultList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SportViewModel(val sportRepository: SportRepository) : ViewModel() {
    // holds the the sport data
    private val _sportResult = MutableLiveData<SportResult>()
    val sportResult: LiveData<SportResult> = _sportResult

    private val sportAppService = SportAppService()

    val cacheSearchResults: MutableLiveData<List<Sport>> by lazy {
        MutableLiveData<List<Sport>>()
    }

    fun searchSport(query: String){
        val results = sportAppService.searchSports(query)
        cacheSearchResults.value = results
    }

    fun track() {
        // launched synchronous job
        viewModelScope.launch(Dispatchers.Main) {
            val result =  sportRepository.getSports()
            if (result is SportResultList.Success) {
                Log.i("6", result.data.toString())
                _sportResult.value = SportResult(success = result.data)
                result.data?.let { sportAppService.setSports(it) }
            } else {
                _sportResult.value = SportResult(error = R.string.register_failed)
            }
        }
    }



}