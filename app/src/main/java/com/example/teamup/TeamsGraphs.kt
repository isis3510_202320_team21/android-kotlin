package com.example.teamup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.FragmentGraphsBinding
import com.example.teamup.databinding.FragmentTeamsGraphsBinding


/**
 * A simple [Fragment] subclass.
 * Use the [TeamsGraphs.newInstance] factory method to
 * create an instance of this fragment.
 */
class TeamsGraphs : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentTeamsGraphsBinding ? = null
    private val binding get() = _binding!!



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        _binding = FragmentTeamsGraphsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}