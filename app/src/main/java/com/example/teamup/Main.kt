package com.example.teamup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.MainLayoutBinding
import com.example.teamup.databinding.MenuLayoutBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match



/**
 * A simple [Fragment] subclass.
 * Use the [Main.newInstance] factory method to
 * create an instance of this fragment.
 */
class Main : Fragment() {


    private var _binding: MainLayoutBinding? = null
    private var _binding2: MenuLayoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val db = FirebaseDatabase.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val auth = Firebase.auth
        auth.currentUser?.reload()
        // Inflate the layout for this fragment
        _binding = MainLayoutBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val auth = Firebase.auth
        auth.currentUser?.reload()

        //TODO: REvisar esto
        //(activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        binding.registerButton.setOnClickListener { findNavController().navigate(R.id.Register) }
        binding.loginButton.setOnClickListener { findNavController().navigate(R.id.loginFragment) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}