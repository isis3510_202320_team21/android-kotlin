package com.example.teamup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.MenuLayoutBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match




class Menu : Fragment() {


    private var _binding: MenuLayoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        (activity as AppCompatActivity?)!!.supportActionBar!!.show()

        // Inflate the layout for this fragment
        _binding = MenuLayoutBinding.inflate(inflater, container, false)
        firebaseAnalytics = Firebase.analytics
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!

        binding.trackButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature"){
                param("type","Track")
            }
        }

        binding.sportsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature"){
                param("type","Sports")
            }
            findNavController().navigate(R.id.sportFragment)
            navView.setCheckedItem(R.id.action_Menu_to_sportFragment)
        }
        /*binding.searchButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Search")
            }
        }*/
        binding.teamsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Teams")
            }
                findNavController().navigate(R.id.action_menu_toTeams)
        }
        binding.routinesButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Routines")
            }
            //findNavController().navigate(R.id.action_Menu_to_itemFragment)
        }
        /*binding.tournamentsButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature") {
                param("type", "Tournaments")
            }
            //findNavController().navigate(R.id.action_Menu_to_itemFragment)
        }*/
        binding.weatherButton.setOnClickListener {
            firebaseAnalytics.logEvent("feature"){
                param( "type", "Weather")
            }
            //findNavController().navigate(R.id.Weather)
        }
        super.onViewCreated(view, savedInstanceState)
        //binding.sportsButton.setOnClickListener { findNavController().navigate(R.id.action_Menu_to_Sports) }
        //binding.teamsButton.setOnClickListener { findNavController().navigate(R.id.action_menu_toTeams) }

    }

//    private fun updateUiWithUser(model: TeamView) {
//        val welcome = model.displayName + " you have met " + model.meetings_7days.toString() + "friend(s) this week!"
//        // TODO : initiate successful logged in experience
//        val appContext = context?.applicationContext ?: return
//        Toast.makeText(appContext, welcome, Toast.LENGTH_LONG).show()
//    }
//
//    private fun showRegisterFailed(@StringRes errorString: Int) {
//        val appContext = context?.applicationContext ?: return
//        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}