package com.example.teamup

import android.Manifest
import android.content.Context
import android.content.Context.LOCALE_SERVICE
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.SearchStagesLayoutBinding
import com.google.android.gms.common.api.GoogleApi
import com.google.android.gms.common.api.internal.GoogleApiManager
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SearchStages : Fragment() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var _binding: SearchStagesLayoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var meetlocation: LatLng
    private lateinit var map: GoogleMap
    private lateinit var locationManager: LocationManager
    private var gpsStatus = false
    private val permissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            showLocation()
        }
    }
    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        showLocation()
        map.setOnMapClickListener {
            newMarker(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SearchStagesLayoutBinding.inflate(inflater, container, false)

        val auth = Firebase.auth
        auth.currentUser?.reload()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(callback)
        binding.confirmBtn.setOnClickListener { findNavController().navigate(R.id.action_searchStages_toTeam) }
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
    }

    private fun showLocation() {
        if (ContextCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            } else {

                permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }
        if (ContextCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
        }
    }


    private fun checkGpsStatus() {
        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun newMarker(p0: LatLng) {
        meetlocation = p0
        map.clear()
        map.addMarker(MarkerOptions().position(p0).title("meeting point"))
    }

}