package com.example.teamup.data.db.DAO

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.teamup.data.db.entities.CategoryEntity
import com.example.teamup.data.db.entities.CategoryWithExercise
import com.example.teamup.data.db.entities.CategoryWithRoutine

@Dao
interface CategoryDAO {
    @Query("SELECT * FROM categories_table")
    suspend fun getAllCategories(): List<CategoryEntity>

    @Query("SELECT * FROM categories_table WHERE name = :name")
    suspend fun getSingleCategoryByName(name: String): CategoryEntity

    @Transaction
    @Query("SELECT * FROM categories_table WHERE category_id = :id")
    suspend fun getSingleCategoryWithExercisesById(id: Int): List<CategoryWithExercise>

    @Transaction
    @Query("SELECT * FROM categories_table WHERE name = :name")
    suspend fun getSingleCategoryWithExercisesByName(name: String): List<CategoryWithExercise>

    @Transaction
    @Query("SELECT * FROM categories_table")
    suspend fun getAllCategoryWithExercises(): List<CategoryWithExercise>

    @Transaction
    @Query("SELECT * FROM categories_table WHERE category_id = :id")
    suspend fun getSingleCategoryWithRoutineById(id: Int): List<CategoryWithRoutine>

    @Transaction
    @Query("SELECT * FROM categories_table WHERE name = :name")
    suspend fun getSingleCategoryWithRoutineByName(name: String): List<CategoryWithRoutine>

    @Transaction
    @Query("SELECT * FROM categories_table")
    suspend fun getAllCategoryWithRoutine(): List<CategoryWithRoutine>


    @Transaction
    @Query("DELETE FROM routine_category_table WHERE category_id = :categoryId")
    suspend fun deleteCategoryFromCrossRef(categoryId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(categories: CategoryEntity):Long

    @Delete
    suspend fun deleteCategory(category: CategoryEntity)

    @Update
    suspend fun updateCategory(category: CategoryEntity)


}