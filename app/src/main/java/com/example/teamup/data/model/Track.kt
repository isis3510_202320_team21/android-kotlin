package com.example.teamup.data.model

data class Track(
        val displayName: String,
        val email: String,
        val meetings_7days : Int
)