package com.example.teamup.data

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.model.CreateTeam
import com.example.teamup.data.model.CreateTournament
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.io.IOException
import java.sql.Date
import java.sql.Time

class CreateTournamentDataSource {
    private var auth: FirebaseAuth =  Firebase.auth
    var user : FirebaseUser? = null
    private val db = Firebase.firestore
    var msg: String = ""

    suspend fun createTournament(name:String, sport:String, date:String, location:String, numTeams:String): Result<CreateTournament>{
        try{
            db.collection("tournaments").document().set(hashMapOf(
                "name" to name,
                "sport" to sport,
                "date" to date,
                "location" to location,
                "numTeams" to numTeams
            )
            ).addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
                msg = exception.message.toString()
            }.await()

            return Result.Success(CreateTournament(name, sport, date, location, numTeams))
        } catch (e: Throwable) {
            return Result.Error(IOException("Error creating tournament", e))
        }
    }


}