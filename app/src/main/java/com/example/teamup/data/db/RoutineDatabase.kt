package com.example.teamup.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.teamup.data.db.DAO.CategoryDAO
import com.example.teamup.data.db.DAO.ExerciseDAO
import com.example.teamup.data.db.DAO.RoutineDAO
import com.example.teamup.data.db.entities.CategoryEntity
import com.example.teamup.data.db.entities.ExerciseEntity
import com.example.teamup.data.db.entities.RoutineCategoryEntity
import com.example.teamup.data.db.entities.RoutineEntity
import com.example.teamup.data.db.entities.RoutineExerciseEntity

@Database(
    entities = [ExerciseEntity::class,
        CategoryEntity::class,
        RoutineEntity::class,
        RoutineExerciseEntity::class,
        RoutineCategoryEntity::class],
    version = 1,
    exportSchema = true
)
abstract class RoutineDatabase:RoomDatabase() {
    abstract fun getExerciseDAO(): ExerciseDAO
    abstract fun getCategoryDAO(): CategoryDAO
    abstract fun getRoutineDAO(): RoutineDAO
}