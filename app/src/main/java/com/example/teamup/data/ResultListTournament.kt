package com.example.teamup.data

import com.example.teamup.data.model.Tournament

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class ResultListTournament<out T : Any> {

    data class Success<out T : Any>(val data: MutableList<Tournament>?) : ResultListTournament<T>()
    data class Error(val exception: Exception) : ResultListTournament<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}