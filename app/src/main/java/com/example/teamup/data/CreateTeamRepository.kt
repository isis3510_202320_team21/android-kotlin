package com.example.teamup.data

import com.example.teamup.data.model.CreateTeam

class CreateTeamRepository (val dataSource: CreateTeamDataSource) {

    suspend fun createTeam (name: String, sport:String, level:String, date: String, time: String, latitude:String, longitude: String): Result<CreateTeam>{
        val result= dataSource.createTeam(name, sport, level, date, time, latitude, longitude)


        return result
    }


}