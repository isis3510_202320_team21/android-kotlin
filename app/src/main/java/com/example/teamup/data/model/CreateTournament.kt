package com.example.teamup.data.model

import com.google.android.gms.maps.model.LatLng
import java.sql.Time
import java.sql.Date

data class CreateTournament (
    val name:String,
    val sport:String,
    val date:String,
    val location:String,
    val numTeams:String
)