package com.example.teamup.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(tableName = "exercises_table")
data class ExerciseEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "exercise_id") val exercise_id:Long = 0L,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "category") val category: String,
    @ColumnInfo(name = "e_category_id") val e_categoryId: Long,
    @ColumnInfo(name = "weight") val weight: Double,
    @ColumnInfo(name = "reps") val reps: Int,
    @ColumnInfo(name = "intensityCounter") val intensityCounter: Int,
    @ColumnInfo(name = "repFlag") val repFlag: Boolean,
    @ColumnInfo(name = "rirFlag") val rirFlag: Boolean,
    @ColumnInfo(name = "comments") val comments: String,
    @ColumnInfo(name = "restTime") val restTime: Long,
    @ColumnInfo(name = "topSet") val topSet: Boolean,
    @ColumnInfo(name = "approxSet") val approxSet: Boolean,
    @ColumnInfo(name = "done") val done: Boolean,
    @ColumnInfo(name = "set_number") val set_number: Int
    // @ColumnInfo(name = "routineId") val routineId: Int
    // hay que ver como inyectar la referencia a la categoria y a rutina
)

