package com.example.teamup.data.model

import android.graphics.drawable.Drawable
import android.location.Location
import java.util.Date

data class Team (
    val name: String?=null,
    val date: String?=null,
    val location: String?=null,
    val level: String?=null,
    val sport:String?=null,
    val latitude:String?=null,
    val longitude:String?=null,
    //val sportIm: Drawable,
)