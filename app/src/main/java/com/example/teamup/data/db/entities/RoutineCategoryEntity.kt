package com.example.teamup.data.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Junction
import androidx.room.Relation

@Entity(tableName = "routine_category_table",
    primaryKeys = ["routine_id", "category_id"],)
data class RoutineCategoryEntity(
    val routine_id:Long,
    val category_id:Long
)

data class RoutineWithCategory(
    @Embedded val routine: RoutineEntity,
    @Relation(
        parentColumn = "routine_id",
        entityColumn = "category_id",
        associateBy = Junction(RoutineCategoryEntity::class)
    )
    val categories: List<CategoryEntity>
)

data class CategoryWithRoutine(
    @Embedded val category: CategoryEntity,
    @Relation(
        parentColumn = "category_id",
        entityColumn = "routine_id",
        associateBy = Junction(RoutineCategoryEntity::class)
    )
    val routines: List<RoutineEntity>
)
