package com.example.teamup.data

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.model.CreateTeam
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.io.IOException
import java.sql.Date
import java.sql.Time

class CreateTeamDataSource {
    private var auth: FirebaseAuth =  Firebase.auth
    var user : FirebaseUser? = null
    private val db = Firebase.firestore
    var msg: String = ""

    suspend fun createTeam(name:String, sport:String, level:String, date:String, time: String, latitude:String, longitude: String): Result<CreateTeam>{
        try{
            db.collection("teams").document(auth.currentUser?.email.toString()).collection("teams").document().set(hashMapOf(
                "name" to name,
                "sport" to sport,
                "level" to level,
                "date" to date,
                "time" to time,
                "latitude" to latitude,
                "longitude" to longitude
            )
            ).addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
                msg = exception.message.toString()
            }.await()

            return Result.Success(CreateTeam(name, sport, level, date, time, latitude, longitude))
        } catch (e: Throwable) {
            return Result.Error(IOException("Error creating team", e))
        }
    }


}