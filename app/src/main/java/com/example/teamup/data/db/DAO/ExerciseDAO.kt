package com.example.teamup.data.db.DAO

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.teamup.data.db.entities.ExerciseEntity
import com.example.teamup.data.db.entities.ExerciseWithRoutine

@Dao
interface ExerciseDAO {
    @Query("SELECT * FROM exercises_table")
    suspend fun getAllExercises(): List<ExerciseEntity>

    @Query("SELECT * FROM exercises_table WHERE name = :name")
    suspend fun getSingleExerciseByName(name: String): ExerciseEntity

    @Query("SELECT * FROM exercises_table WHERE exercise_id = :id")
    suspend fun getSingleExerciseById(id: Int): ExerciseEntity

    @Transaction
    @Query("SELECT * FROM exercises_table WHERE exercise_id = :id")
    suspend fun getExercisesWithRoutineById(id: Int): List<ExerciseWithRoutine>

    @Transaction
    @Query("SELECT * FROM exercises_table WHERE name = :name")
    suspend fun getExercisesWithRoutineByName(name: String): List<ExerciseWithRoutine>

    @Transaction
    @Query("SELECT * FROM exercises_table WHERE e_category_id = :category_id")
    suspend fun getExercisesForCategory(category_id: Long): List<ExerciseEntity>

    @Transaction
    @Query("SELECT * FROM exercises_table")
    suspend fun getAllExercisesWithRoutine(): List<ExerciseWithRoutine>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertExercise(exercises: List<ExerciseEntity>): List<Long>

    @Update
    suspend fun updateExercise(exercise: ExerciseEntity)

    @Delete
    suspend fun deleteExercise(exercise: ExerciseEntity)

}