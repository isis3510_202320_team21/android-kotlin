package com.example.teamup.data.model

import com.google.android.gms.maps.model.LatLng
import java.sql.Time
import java.sql.Date

data class CreateTeam (
    val name:String,
    val sport:String,
    val level:String,
    val date:String,
    val time:String,
    val latitude:String,
    val longitude: String
)