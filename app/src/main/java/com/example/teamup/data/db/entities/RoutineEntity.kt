package com.example.teamup.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "routines_table")
data class RoutineEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "routine_id") val routine_id: Long = 0L,
    @ColumnInfo(name = "rating") val rating: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "duration") val duration: Int,
    @ColumnInfo(name = "done") val done: Boolean,
    @ColumnInfo(name = "totalSets") val totalSets: Int,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "description") val description: String,
    // val exercises: List<ExerciseEntity>
    // user ref

)


