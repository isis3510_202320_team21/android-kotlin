package com.example.teamup.data

import android.util.Log
import com.example.teamup.data.model.Tournament
import com.example.teamup.data.model.Track
import com.example.teamup.data.model.TrackTournament
import com.example.tournamentup.data.TournamentDataSource
import java.util.*


class TournamentRepository (val dataSource: TournamentDataSource){


    // in-memory cache of the loggedInUser object
    /*var team: Tournament? = null
        private set

    val isLoggedIn: Boolean
        get() = team != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        team = null
    }

    fun logout() {
        team = null
        dataSource.logout()
    }*/

    suspend fun getTournaments(): ResultListTournament<MutableList<Tournament>> {
        // handle request
        val result = dataSource.getTournaments()

        return result
    }

    suspend fun track(tour: String): Result<TrackTournament>{
        val result = dataSource.track(tour)

        return result
    }


    suspend fun removeTournament(name:String){
        // handle request
        dataSource.removeTournament(name)
    }
}