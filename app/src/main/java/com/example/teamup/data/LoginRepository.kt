package com.example.teamup.data

import android.content.ContentValues.TAG
import android.util.Log
import com.example.teamup.data.model.LoggedInUser
import com.example.teamup.data.model.Track
import java.util.Calendar

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(val dataSource: LoginDataSource) {

    // in-memory cache of the loggedInUser object
    var user: LoggedInUser? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun logout() {
        user = null
        dataSource.logout()
    }

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        // handle login
        val result = dataSource.login(username, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    suspend fun register(username: String, firstName: String, lastName: String, email: String, password: String): Result<LoggedInUser> {
        // handle login
        val result = dataSource.register(username, firstName,lastName,email, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    suspend fun track(): Result<Track>{
        val result = dataSource.track()

        return result
    }

    suspend fun trackBack(time:Calendar): Result<Track>{
        val result = dataSource.trackBack(time)
        return result
    }
}