package com.example.teamup.data.model

import javax.annotation.Nullable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser(
    val userId: String,
    val displayName: String,
    val email: String,
    val username: String,
)