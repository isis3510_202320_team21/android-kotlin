package com.example.teamup.data.mapper

import com.example.teamup.data.db.entities.CategoryEntity
import com.example.teamup.data.db.entities.ExerciseEntity
import com.example.teamup.data.db.entities.RoutineEntity
import com.example.teamup.routine.Routine
import com.example.teamup.routine.category.Category
import com.example.teamup.routine.exercise.Exercise

class ModelMapper {
    fun categoryToCategoryEntity(category: Category): CategoryEntity {
        return CategoryEntity(
            color = category.colorString?:"" ,
            name = category.name?:"",
            uid = category.uid?:""
        )
    }

    fun categoryEntityToCategory(categoryEntity: CategoryEntity, exercises: MutableList<ExerciseEntity>): Category {
        return Category(
            colorString = categoryEntity.color,
            name = categoryEntity.name,
            uid = categoryEntity.uid,
            exercises = exercises.map { exerciseEntityToExercise(it) }
        )
    }

    fun exerciseEntityToExercise(exerciseEntity: ExerciseEntity): Exercise{
        return Exercise(
            date = exerciseEntity.date,
            name = exerciseEntity.name,
            category = exerciseEntity.category,
            weight = exerciseEntity.weight,
            reps = exerciseEntity.reps,
            intensityCounter = exerciseEntity.intensityCounter,
            repFlag = exerciseEntity.repFlag,
            rirFlag = exerciseEntity.rirFlag,
            comments = exerciseEntity.comments,
            restime = exerciseEntity.restTime,
            topSet = exerciseEntity.topSet,
            approxSet = exerciseEntity.approxSet,
            done = exerciseEntity.done,
            setNumber = exerciseEntity.set_number
        )
    }

    fun exerciseToExerciseEntity(exercise: Exercise, categoryId: Long):ExerciseEntity{
        return ExerciseEntity(
            date = exercise.date?:"",
            name = exercise.name?:"",
            category = exercise.category?:"",
            weight = exercise.weight?:0.0,
            reps = exercise.reps?:0,
            intensityCounter = exercise.intensityCounter?:0,
            repFlag = exercise.repFlag?:false,
            rirFlag = exercise.rirFlag?:false,
            comments = exercise.comments?:"",
            restTime = exercise.restime?:0,
            topSet = exercise.topSet?:false,
            approxSet = exercise.approxSet?:false,
            done = exercise.done?:false,
            e_categoryId = categoryId,
            set_number = exercise.setNumber?:0
        )
    }

    fun routineToRoutineEntity(routine: Routine): RoutineEntity {
        return RoutineEntity(
            name= routine.name?:"",
            rating = routine.rating?:0,
            duration = routine.duration?:0,
            done = routine.done?:false,
            totalSets = routine.totalSets?:0,
            date = routine.date?:"",
            description = routine.description?:"",
        )
    }
    fun routineEntityToRoutine(routineEntity: RoutineEntity, exercises: MutableList<ExerciseEntity>): Routine {
        return Routine(
            name= routineEntity.name,
            rating = routineEntity.rating,
            duration = routineEntity.duration,
            done = routineEntity.done,
            totalSets = routineEntity.totalSets,
            date = routineEntity.date,
            description = routineEntity.description,
            exercises = exercises.map { exerciseEntityToExercise(it) }
        )
    }
}