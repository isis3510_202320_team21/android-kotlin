package com.example.teamup.data

import android.util.Log
import com.example.teamup.data.model.Team

class TeamRepository (val dataSource: TeamDataSource){


    // in-memory cache of the loggedInUser object
    /*var team: Team? = null
        private set

    val isLoggedIn: Boolean
        get() = team != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        team = null
    }

    fun logout() {
        team = null
        dataSource.logout()
    }*/

    suspend fun getTeams(): ResultList<MutableList<Team>> {
        // handle request
        val result = dataSource.getTeams()

        return result
    }

    suspend fun removeTeam(name:String){
        // handle request
        dataSource.removeTeam(name)
    }
}