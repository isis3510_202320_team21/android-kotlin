package com.example.teamup.data.db.DAO;

import androidx.room.Dao;
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query;
import androidx.room.Transaction
import androidx.room.Update

import com.example.teamup.data.db.entities.RoutineEntity;
import com.example.teamup.data.db.entities.RoutineWithCategory
import com.example.teamup.data.db.entities.RoutineWithExercise

@Dao
interface RoutineDAO {
    @Query("SELECT * FROM routines_table")
    suspend fun getAllRoutines():List<RoutineEntity>

    @Transaction
    @Query("SELECT * FROM routines_table WHERE routine_id = :id")
    suspend fun getRoutineWithCategoriesById(id: Int): List<RoutineWithCategory>

    @Transaction
    @Query("SELECT * FROM routines_table WHERE name = :name")
    suspend fun getRoutineWithCategoriesByName(name: String): List<RoutineWithCategory>

    @Transaction
    @Query("SELECT * FROM routines_table")
    suspend fun getAllRoutineWithCategories(): List<RoutineWithCategory>

    @Transaction
    @Query("SELECT * FROM routines_table WHERE routine_id = :id")
    suspend fun getRoutineWithExerciseById(id: Int): List<RoutineWithExercise>

    @Transaction
    @Query("SELECT * FROM routines_table WHERE name = :name")
    suspend fun getRoutineWithExerciseByName(name: String): List<RoutineWithExercise>

    @Transaction
    @Query("SELECT * FROM routines_table")
    suspend fun getAllRoutineWithExercise(): List<RoutineWithExercise>

    @Transaction
    @Query("SELECT * FROM routines_table WHERE name = :name")
    suspend fun getSingleRoutineByName(name: String): RoutineEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRoutine(routines: List<RoutineEntity>)

    @Delete
    suspend fun deleteRoutine(routine: RoutineEntity)

    @Update
    suspend fun updateRoutine(routine: RoutineEntity)
}
