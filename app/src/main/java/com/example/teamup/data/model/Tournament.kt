package com.example.teamup.data.model

import android.graphics.drawable.Drawable
import android.location.Location
import java.util.Date

data class Tournament (
    val name: String?=null,
    val date: String?=null,
    val location: String?=null,
    val sport:String?=null,
    val numTeams:String?=null,
    //val sportIm: Drawable,
)