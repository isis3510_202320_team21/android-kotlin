package com.example.teamup.data

import com.example.teamup.sport.Sport


/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class SportResultList<out T : Any> {
    // invariant, covariant, contravariant i aint have no time to learn allat, shiii
    data class Success<out T : Any>(val data: MutableList<Sport>?) : SportResultList<T>()
    data class Error(val exception: Exception) : SportResultList<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}