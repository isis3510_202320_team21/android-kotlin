package com.example.teamup.data

import com.example.teamup.data.model.CreateTeam
import com.example.teamup.data.model.CreateTournament

class CreateTournamentRepository (val dataSource: CreateTournamentDataSource) {

    suspend fun createTournament (name: String, sport:String,  date: String, location:String, numTeams:String): Result<CreateTournament>{
        val result= dataSource.createTournament(name, sport, date, location, numTeams)


        return result
    }


}