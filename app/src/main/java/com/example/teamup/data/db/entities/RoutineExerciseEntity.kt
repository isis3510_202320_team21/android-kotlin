package com.example.teamup.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.Relation

@Entity(tableName = "routine_exercises_table",
    primaryKeys = ["routine_id", "exercise_id"],)
data class RoutineExerciseEntity(
    @ColumnInfo(name = "routine_id")val routine_id: Long,
    @ColumnInfo(name = "exercise_id")val exercise_id: Long
)

data class RoutineWithExercise(
    @Embedded val routine: RoutineEntity,
    @Relation(
        parentColumn = "routine_id",
        entityColumn = "exercise_id",
        associateBy = Junction(RoutineExerciseEntity::class)
    )
    val exercises: List<ExerciseEntity>
)

data class ExerciseWithRoutine(
    @Embedded val exercise: ExerciseEntity,
    @Relation(
        parentColumn = "exercise_id",
        entityColumn = "routine_id",
        associateBy = Junction(RoutineExerciseEntity::class)
    )
    val routines: List<RoutineEntity>
)