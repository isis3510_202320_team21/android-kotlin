package com.example.teamup.data.model

data class Weather (
    val cityName: String?=null,
    val weather: String?=null,
    val temp: String?=null,
    val msg: String?=null

)