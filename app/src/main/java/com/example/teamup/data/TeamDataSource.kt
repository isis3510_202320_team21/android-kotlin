package com.example.teamup.data

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.model.Team
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.ArrayList
import kotlin.math.log

class TeamDataSource {

    private var auth: FirebaseAuth =  Firebase.auth
    var user : FirebaseUser? = null
    private val db = Firebase.firestore
    private lateinit var teamsList: MutableList<Team>

    suspend fun getTeams(): ResultList<MutableList<Team>> {

        var msg: String = ""
        teamsList = ArrayList()

        val email = auth.currentUser?.email.toString()

        val dbRef = db.collection("teams")

        dbRef.document(email).collection("teams").get().addOnSuccessListener {
            if(!it.isEmpty){
                for(data in it.documents){
                    val team:Team? = data.toObject(Team::class.java)
                    if (team != null) {
                        Log.i("1", team.name.toString())
                        teamsList.add(team)
                        Log.i("1", teamsList.size.toString())
                    }
                }

            }
        }.addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }.await()

        if (msg=="" && teamsList.size > 0) {
            Log.i("3", teamsList.get(0).name.toString())
            return ResultList.Success(teamsList)
        } else if(teamsList.size == 0){
            return ResultList.Success(teamsList)
        }
        else {
            return ResultList.Error(Exception("Error logging in$msg"))
        }
    }

    suspend fun removeTeam(name:String) {

        var msg: String = ""
        teamsList = ArrayList()

        val email = auth.currentUser?.email.toString()

        val dbRef = db.collection("teams").document(email).collection("teams")

        dbRef.whereEqualTo("name",name).get().addOnSuccessListener {
            if(!it.isEmpty){
                for(data in it.documents){
                    dbRef.document(data.id).delete()
                }
            }

        }.addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }
    }

}