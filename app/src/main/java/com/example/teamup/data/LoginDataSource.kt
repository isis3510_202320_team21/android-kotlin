package com.example.teamup.data

import android.content.ContentValues.TAG
import android.util.Log
import com.example.teamup.data.model.LoggedInUser
import com.example.teamup.data.model.Track
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.io.IOException
import java.lang.Exception
import java.util.*


/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {
    private var auth: FirebaseAuth =  Firebase.auth
    var user : FirebaseUser? = null
    private val db = Firebase.firestore

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        // Initialize Firebase Auth
        // TODO: handle loggedInUser authentication

        var email: String? = null
        var displayName_: String? = null
        var msg: String = ""

        try {
            db.collection("users").document(username).get().addOnSuccessListener {
                email =it.data?.get("email").toString()
                displayName_ =it.data?.get("first").toString()+" "+ it.data?.get("last").toString()
                //the log show the correct nickname
                Log.i("usuari_query", email.toString())
            }.addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
                msg = exception.message.toString()
            }.await()

            auth.signInWithEmailAndPassword(email!!, password).addOnCompleteListener{ task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        user = auth.currentUser
                        Log.d(TAG, auth.currentUser.toString())
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        //Toast.makeText(this, "Authentication failed.",
                            //Toast.LENGTH_SHORT).show()
                        msg= task.exception?.message.toString()
                    }
                }.await()

            val profileUpdates = userProfileChangeRequest {
                displayName = displayName_
            }
            user!!.updateProfile(profileUpdates).await()

            if (user != null && msg=="") {
                Log.d(TAG, user?.uid.toString())
                val fakeUser = LoggedInUser(user?.uid.toString(), displayName_.toString(),email.toString(), username)
                return Result.Success(fakeUser)
            } else {
                return Result.Error(Exception("Error logging in"))
            }
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    suspend fun register(username: String, firstName: String, lastName: String, email: String, password: String): Result<LoggedInUser> {
        // Initialize Firebase Auth
        // TODO: handle loggedInUser authentication
        var msg: String = ""
        try {
            // TODO: handle loggedInUser authentication

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener{ task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    user = auth.currentUser

                    Log.d(TAG, auth.currentUser.toString())
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    //Toast.makeText(this, "Authentication failed.",
                    //Toast.LENGTH_SHORT).show()
                    msg= task.exception?.message.toString()
                    Log.i("!!!!", msg)
                }
            }.await()

            val profileUpdates = userProfileChangeRequest {
                displayName = firstName+" "+lastName
            }
            user!!.updateProfile(profileUpdates).await()

            db.collection("users").document(username).set(
                hashMapOf(
                    "username" to username,
                    "first" to firstName,
                    "last" to lastName,
                    "email" to email
                )
            ).addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                msg = exception.message.toString()
            }.await()

            db.collection("tracks").document(email).set(
                hashMapOf(
                    "email" to email
                )
            ).addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                msg = exception.message.toString()
            }.await()

            if (user != null && msg=="") {
                Log.d(TAG, user?.uid.toString())
                val fakeUser = LoggedInUser(user?.uid.toString(), firstName+" "+lastName, user?.email.toString(), username)
                return Result.Success(fakeUser)
            } else {
                Log.i("Entra E!!!", msg)
                return Result.Error(Exception("Error registering: $msg"))
            }
        } catch (e: Throwable) {
            return Result.Error(IOException("Error registering: $msg", e))
        }
    }

    suspend fun track(): Result<Track> {

        var msg: String = ""
        var count = 0

        val email = auth.currentUser?.email.toString()
        val displayName = auth.currentUser?.displayName.toString()

//        db.collection("tracks").document(email).update(
//                "email", email
//        ).addOnFailureListener { exception ->
//            Log.w(TAG, "Error getting documents.", exception)
//            msg = exception.message.toString()
//        }.await()

        val time = Calendar.getInstance()
        val current =time.time

        val before = getDaysAgo(7)

        db.collection("tracks").document(email).update("date", FieldValue.arrayUnion(current)
        ).addOnFailureListener { exception ->
            Log.w(TAG, "Error getting documents.", exception)
            msg = exception.message.toString()
        }.await()

        val dbRef = db.collection("tracks")


        val query = dbRef.document(email).get().addOnSuccessListener {
            val dates = it.data?.get("date") as List<Timestamp>
            //the log show the correct nickname
            count = dates.filter{it.toDate().after(before) && it.toDate().before(Date())}.size
            Log.i("usuari_query", count.toString()+ " de " + dates.size.toString())
        }.addOnFailureListener { exception ->
            Log.w(TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }.await()
//        val countQuery = query.count()
//        countQuery.get(AggregateSource.SERVER).addOnCompleteListener { task ->
//            if (task.isSuccessful) {
//                // Count fetched successfully
//                val snapshot = task.result
//                count = snapshot.count.toInt()
//                Log.d(TAG, "Count: ${snapshot.count}")
//            } else {
//                Log.d(TAG, "Count failed: ", task.getException())
//                msg = task.exception?.message.toString()
//
//            }
//        }.await()

        if (msg=="") {
            Log.d(TAG, current.toString())
            //Filter and count
            val fakeUser = Track(displayName, email, count)
            return Result.Success(fakeUser)
        } else {
            return Result.Error(Exception("Error logging in$msg"))
        }


    }

    fun getDaysAgo(daysAgo: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)

        return calendar.time
    }

    suspend fun trackBack(time:Calendar): Result<Track> {

        var msg: String = ""
        var count = 0

        val email = auth.currentUser?.email.toString()
        val displayName = auth.currentUser?.displayName.toString()

        val current =time.time

        val before = getDaysAgo(7)

        db.collection("tracks").document(email).update("date", FieldValue.arrayUnion(current)
        ).addOnFailureListener { exception ->
            Log.w(TAG, "Error getting documents.", exception)
            msg = exception.message.toString()
        }.await()

        val dbRef = db.collection("tracks")


        val query = dbRef.document(email).get().addOnSuccessListener {
            val dates = it.data?.get("date") as List<Timestamp>
            //the log show the correct nickname
            count = dates.filter{it.toDate().after(before) && it.toDate().before(Date())}.size
            Log.i("usuari_query", count.toString()+ " de " + dates.size.toString())
        }.addOnFailureListener { exception ->
            Log.w(TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }.await()

        if (msg=="") {
            Log.d(TAG, current.toString())
            //Filter and count
            val fakeUser = Track(displayName, email, count)
            return Result.Success(fakeUser)
        } else {
            return Result.Error(Exception("Error logging in$msg"))
        }


    }



    fun logout() {
        // TODO: revoke authentication
        auth = Firebase.auth
        auth.signOut()
    }
}