package com.example.tournamentup.data

import android.content.ContentValues
import android.util.Log
import com.example.teamup.data.Result
import com.example.teamup.data.ResultListTournament
import com.example.teamup.data.model.*
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class TournamentDataSource {

    private var auth: FirebaseAuth =  Firebase.auth
    var user : FirebaseUser? = null
    private val db = Firebase.firestore
    private lateinit var tournamentsList: MutableList<Tournament>

    suspend fun getTournaments(): ResultListTournament<MutableList<Tournament>> {

        var msg: String = ""
        tournamentsList = ArrayList()

        val email = auth.currentUser?.email.toString()

        val dbRef = db.collection("tournaments")

        dbRef.get().addOnSuccessListener {
            if(!it.isEmpty){
                for(data in it.documents){
                    val tournament:Tournament? = data.toObject(Tournament::class.java)
                    if (tournament != null) {
                        Log.i("1", tournament.name.toString())
                        tournamentsList.add(tournament)
                        Log.i("1", tournamentsList.size.toString())
                    }
                }

            }
        }.addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }.await()

        if (msg=="" && tournamentsList.size > 0) {
            Log.i("3", tournamentsList.get(0).name.toString())
            return ResultListTournament.Success(tournamentsList)
        } else if(tournamentsList.size == 0){
            return ResultListTournament.Success(tournamentsList)
        }
        else {
            return ResultListTournament.Error(Exception("Error logging in$msg"))
        }
    }

    suspend fun track(tour: String): Result<TrackTournament> {

        var msg: String = ""
        var count = 0

        var tourList = ArrayList<Int>()
        val tourNames = ArrayList<String>()

        val dbRef = db.collection("tournament_track")

        dbRef.document(tour).update("num", FieldValue.increment(1)
        ).addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents.", exception)
            msg = exception.message.toString()
        }.await()

        dbRef.get().addOnSuccessListener {
            if(!it.isEmpty){
                for(data in it.documents){
                    val tour: TrackTournamentDatabase? = data.toObject(TrackTournamentDatabase::class.java)
                    Log.i("AAAAAAAAAAAAAA",data.toString())
                    if (tour != null) {
                        Log.i("1", tour.toString())
                        tourList.add(tour.num!!.toInt())
                        tourNames.add(data.id)
                    }
                }
            }
        }.addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }.await()

        val max = tourList.maxOrNull() ?: 0
        val idx = tourList.indexOf(max)
        if(idx==-1){
            msg = "Error recuperando la información"
        }

        if (msg=="") {
            //Filter and count
            val popularTour = tourNames.get(idx)
            val fakeUser = TrackTournament(popularTour)
            return Result.Success(fakeUser)
        } else {
            return Result.Error(Exception("Error $msg"))
        }

    }
    suspend fun removeTournament(name:String) {

        var msg: String = ""
        tournamentsList = ArrayList()

        val email = auth.currentUser?.email.toString()

        val dbRef = db.collection("tournaments")

        dbRef.whereEqualTo("name",name).get().addOnSuccessListener {
            if(!it.isEmpty){
                for(data in it.documents){
                    dbRef.document(data.id).delete()
                }
            }

        }.addOnFailureListener { exception ->
            Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            msg = exception.message.toString()
        }
    }

}