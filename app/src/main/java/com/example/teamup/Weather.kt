package com.example.teamup

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.lifecycleScope
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.Volley
import com.example.teamup.databinding.FragmentWeatherBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import org.json.JSONObject
import java.net.URL
import java.util.*
import kotlin.properties.Delegates

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER



class Weather : Fragment() {
    // TODO: Rename and change types of parameters

    private var _binding: FragmentWeatherBinding? = null
    private val binding get() = _binding!!
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionIdLocation = 1
    private var lat by Delegates.notNull<Double>()
    private var long by Delegates.notNull<Double>()
    private lateinit var url: String
    private lateinit var cld : ConnectionLiveData
    private lateinit var alertDialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //Network checking
        checkNetworkConnection()
        val appContext = context?.applicationContext
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.weather_label)
        //set message for alert dialog
        builder.setMessage("You are currently offline, check your internet connection to continue")
        builder.setIcon(R.drawable.baseline_wifi_off_24)
        builder.setPositiveButton("Go back"){dialogInterface , which ->
            activity?.onBackPressed()
        }
        // Create the AlertDialog
        alertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)

        if (!checkForInternet(appContext!!)) {
            Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
            alertDialog.show()
        }
        // Inflate the layout for this fragment
        _binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        getLocation()
        //getWeather()

    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionIdLocation
        )
    }
    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == permissionIdLocation) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLocation()
            }
        }
    }

    @SuppressLint("MissingPermission", "SetTextI18n")
    private fun getLocation() {
        if (checkPermissions()) {

            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    val location: Location? = task.result
                    if (location != null) {
                        val queue = Volley.newRequestQueue(requireActivity())

                        val geocoder = Geocoder(requireActivity(), Locale.getDefault())
                        val list: List<Address> =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1,  ) as List<Address>
                        lat = list[0].latitude
                        long = list[0].longitude
                        url = "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&units=metric&appid=74d1551d47bea8784971fa354ba61b9b"

  //  }
    // private fun getWeather(){
         //val queue = Volley.newRequestQueue(requireActivity())
            val cityName = "Bogota"
         //url = "https://api.openweathermap.org/data/2.5/weather?lat=4.6097&lon=-74.0817&units=metric&appid=74d1551d47bea8784971fa354ba61b9b"

         //val url = "https://api.openweathermap.org/data/2.5/weather?lat=4.6097&lon=-74.0817&units=metric&appid=74d1551d47bea8784971fa354ba61b9b"
            val jsonRequest = JsonObjectRequest(url, { response ->
                val main = response.getJSONObject("main")
                val temp = main.getString("temp")
                val tempDeg = "$temp°C"
                //binding.cityName.text = cityName
                binding.cityName.text = list[0].locality
                binding.temp.text = tempDeg

                val weather = response.getJSONArray("weather")
                val weath = weather.getJSONObject(0).getString("main")
                if (weath == "Clear"){
                    Picasso.get().load("https://openweathermap.org/img/wn/01d@2x.png").into(binding.weatherIcon)
                }
                if (weath == "Clouds"){
                    Picasso.get().load("https://openweathermap.org/img/wn/02d@2x.png").into(binding.weatherIcon)
                }
                if (weath == "Thunderstorm"){
                    Picasso.get().load("https://openweathermap.org/img/wn/11d@2x.png").into(binding.weatherIcon)
                }
                if (weath == "Drizzle"){
                    Picasso.get().load("https://openweathermap.org/img/wn/09d@2x.png").into(binding.weatherIcon)

                }
                if (weath == "Rain"){
                    Picasso.get().load("https://openweathermap.org/img/wn/10d@2x.png").into(binding.weatherIcon)

                }
                if (weath == "Snow"){
                    Picasso.get().load("https://openweathermap.org/img/wn/13d@2x.png").into(binding.weatherIcon)

                }
                if (weath in arrayOf("Mist", "Smoke", "Haze", "Dust", "Fog", "Sand", "Dust", "Ash", "Squall", "Tornado")){
                    Picasso.get().load("https://openweathermap.org/img/wn/50d@2x.png").into(binding.weatherIcon)

                }

                if ((weath == "Clouds" || weath == "Clear") && (temp.toFloat() in 5.0..25.0)){
                    binding.res.text= "You can do sport outside! :)"
                } else {
                    binding.res.text= "You can't do sport outside the weather is too bad :'("
                }

            }, {
                //binding.cityName.text = cityName
                binding.temp.text = "error"
            })
         queue.add(jsonRequest)


                    }
                }
            } else {
                Toast.makeText(requireActivity(), "Please turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
     }

    private fun checkNetworkConnection() {
        val appContext = context?.applicationContext ?: return

        cld = ConnectionLiveData(requireActivity().application)

        cld.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                Toast.makeText(appContext, "Connected", Toast.LENGTH_SHORT).show()
                alertDialog.hide()
            } else {
                Toast.makeText(appContext, "Disconnected", Toast.LENGTH_SHORT).show()
                alertDialog.show()
            }
        }
    }

    private fun checkForInternet(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val network = connectivityManager.activeNetwork ?: return false

            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}