package com.example.teamup.routine.category

sealed class CategoryResultFb<out T>{
    data class Success<out T>(val data: T) : CategoryResultFb<T>()
    data class Error(val exception: Exception) : CategoryResultFb<Nothing>()
}
