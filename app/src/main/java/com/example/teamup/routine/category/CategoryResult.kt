package com.example.teamup.routine.category

class CategoryResult (
    val success: Any? = null,
    val error: Int? = null
)