package com.example.teamup.routine.exercise

import android.content.pm.PackageManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.NetworkUtils
import com.example.teamup.R
import com.example.teamup.databinding.FragmentExerciseBinding
import com.example.teamup.databinding.FragmentExerciseListBinding
import com.example.teamup.sport.teamsList.SportTeamsFragmentArgs
import com.example.teamup.ui.team.TeamsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class ExerciseListFragment : Fragment() {

    private lateinit var exerciseListListenerRegistration: ListenerRegistration

    private var columnCount = 1
    private var _binding: FragmentExerciseListBinding? = null
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics


    private lateinit var mFusedLocationClient : FusedLocationProviderClient
    private val permissionId = 2

    private var msg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentExerciseListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        val args = bundle?.let { ExerciseListFragmentArgs.fromBundle(it) }
        val categoryName = args?.categoryName
        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
//        getLocation()
        if (!NetworkUtils.isNetworkAvailable(requireContext())) {
            AlertDialog.Builder(requireContext())
                .setTitle("No Internet Connection")
                .setMessage("You're not connected to the internet. The app will use local data.")
                .setPositiveButton(android.R.string.ok) { dialog, which ->
                    // Do something when the user acknowledges the dialog
                }
                .show()
        }

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        val exerciseViewModel = ViewModelProvider(this, ExerciseViewModelFactory())
            .get(ExerciseListViewModel::class.java)
        exerciseViewModel.track(categoryName.toString())
        //change legs for the arg coming from the other view

        exerciseViewModel.exerciseResult.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result.error?.let {
                showRegisterFailed(it)
            }
            result.success?.let {
                updateUiWithUser(it as ArrayList<Exercise>)
            }
        }

        exerciseViewModel.cacheSearchResults.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result?.let {
                updateUiWithUser(it as ArrayList<Exercise>)
            }
        }

        binding.searchCategoryBtn.setOnClickListener {
            val query = binding.inputSearchExercise.text.toString()
            exerciseViewModel.searchExercises(query)
        }
        // mirar la creacion de acciones en el xml
        //binding.createTeamBtn.setOnClickListener { teamViewModel.track()}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
        binding.title.text = "Exercises for $categoryName"


    }

    private fun updateUiWithUser(model: ArrayList<Exercise>) {
        val appContext = context?.applicationContext ?: return

        val recyclerView: RecyclerView = binding.exerciseList

        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                var adapter = ExcersiceListItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : ExcersiceListItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        //BQ deporte mas popular
                        firebaseAnalytics.logEvent("Exercise"){
                            param("Exercise", model[position].name.toString())
                        }
                        Log.i("TEMP", msg)

                        val builder = AlertDialog.Builder(requireActivity())
                        //set title for alert dialog
                        builder.setTitle(model[position].name)
                        //set message for alert dialog
                        var message = "You just tapped on "+ model[position].name +"!"
                        builder.setIcon(R.drawable.munequito)

                        //performing positive action
                        //performing cancel action
                        builder.setPositiveButton("Ok"){dialogInterface , which ->

                        }


                        //findNavController().navigate(R.id.sportTeamsFragment)
                        val exercise = model[position]
                        val action = ExerciseListFragmentDirections.actionExerciseListFragmentToExerciseFragment2(exercise)
//
                        findNavController().navigate(action)


//                        NavigationView.(R.id.action_sportFragment_to_sportTeamsFragment)
                    }
                })
            }
        }
    }

    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }


    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TeamsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

}