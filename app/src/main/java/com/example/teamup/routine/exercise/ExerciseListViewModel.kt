package com.example.teamup.routine.exercise

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ExerciseListViewModel(val exerciseRepository: ExerciseRepository) : ViewModel() {

    private var _exerciseResult = MutableLiveData<ExerciseResult>()
    val exerciseResult: LiveData<ExerciseResult> = _exerciseResult

    private val exerciseAppService = ExerciseAppService()

    val cacheSearchResults: MutableLiveData<List<Exercise>> by lazy {
        MutableLiveData<List<Exercise>>()
    }

    fun searchExercises(query: String){
        val results = exerciseAppService.searchExercise(query)
        cacheSearchResults.value = results
    }

    fun filterExercises(query: String){
        val results = exerciseAppService.filterExerciseByCategory(query)
        cacheSearchResults.postValue(results)
    }

    fun track(categoryName: String){
        viewModelScope.launch(Dispatchers.IO) {
            val result= exerciseRepository.getExercises()
            val sortedResult = withContext(Dispatchers.Default){
                when(result){
                    is ExerciseResultList.Success -> {
                        result.data?.let{
                            exerciseAppService.setExercise(it)
                            filterExercises(categoryName)
                            Log.i("6", exerciseAppService.getExercise().toString())
                        }
                        ExerciseResultList.Success(cacheSearchResults.value as MutableList<Exercise>?)
                    }
                    else -> result
                }
            }
            withContext(Dispatchers.Main){
                when(sortedResult){
                    is ExerciseResultList.Success -> {
                        Log.i("6", sortedResult.data.toString())
                        _exerciseResult.value = ExerciseResult(success = sortedResult.data)
                        sortedResult.data?.let { exerciseAppService.setExercise(it)}
                    }
                    else -> {
                        _exerciseResult.value = ExerciseResult(error = R.string.load_failed)
                    }
                }
            }
        }
    }
}