package com.example.teamup.routine.exercise

class ExerciseAppService {
    private var exerciseList: List<Exercise> = listOf()

    @Synchronized
    fun setExercise(exercise: List<Exercise>) {
        exerciseList = exercise.toList() // Make a defensive copy of the list
    }
    @Synchronized
    fun addExercise(exercise: Exercise) :ExerciseResultList<MutableList<Exercise>> {
        exerciseList = exerciseList + exercise
        val list_mut = exerciseList.toMutableList()
        return ExerciseResultList.Success(list_mut)
    }
    fun searchExercise(query:String): List<Exercise>{
        return exerciseList.filter { exercise -> exercise.name?.contains(query, ignoreCase=true)?: false }
    }
    fun filterExerciseByCategory(query:String): List<Exercise>{
        val filtered = exerciseList.filter { exercise -> exercise.category?.contains(query, ignoreCase=true)?: false }
        return filtered
    }

    fun getExercise(): List<Exercise>{
        return exerciseList
    }
    fun size(): Int{
        return exerciseList.size
    }
}