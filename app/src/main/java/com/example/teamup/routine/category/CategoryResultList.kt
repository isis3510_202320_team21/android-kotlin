package com.example.teamup.routine.category

import com.example.teamup.routine.exercise.ExerciseResultList


sealed class CategoryResultList<out T: Any> {

    data class Success<out T:Any>(val data: MutableList<Category>): CategoryResultList<T>()
    data class Error(val exception: Exception): CategoryResultList<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }

}