package com.example.teamup.routine

class RoutineResult (
    val success: Any? = null,
    val error: Int? = null
)