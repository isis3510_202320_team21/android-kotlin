package com.example.teamup.routine.exercise

import android.content.pm.PackageManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.NetworkUtils
import com.example.teamup.R
import com.example.teamup.databinding.FragmentExerciseBinding
import com.example.teamup.ui.team.TeamsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Date



class ExerciseFragment : Fragment() {

    private lateinit var exerciseListenerRegistration: ListenerRegistration
    private var countDownTimer: CountDownTimer? = null

    private var columnCount = 1
    private var _binding: FragmentExerciseBinding? = null
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics


    private lateinit var mFusedLocationClient : FusedLocationProviderClient
    private val permissionId = 2

    private var msg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentExerciseBinding.inflate(inflater, container, false)
        return binding.root

    }

    private fun showIntensityDialog(exercise: Exercise) {
        // Inflate the custom layout
        val layoutInflater = LayoutInflater.from(context)
        val intensityDialogLayout = layoutInflater.inflate(R.layout.intensity_layout, null)
        val exerciseViewModel = ViewModelProvider(this, ExerciseViewModelFactory())
            .get(ExerciseViewModel::class.java)

        // Create the AlertDialog
        val intensityDialog = AlertDialog.Builder(requireContext())
            .setView(intensityDialogLayout)
            .setTitle("Intensity")
            .setPositiveButton("OK") { dialog, _ ->
                val inputField = intensityDialogLayout.findViewById<EditText>(R.id.intensity_input)
                val rirCheckbox = intensityDialogLayout.findViewById<RadioButton>(R.id.rir_radio)
                val repCheckbox = intensityDialogLayout.findViewById<RadioButton>(R.id.rep_radio)
                val topSetRadio = intensityDialogLayout.findViewById<RadioButton>(R.id.top_set_radio)
                val approximationSetRadio = intensityDialogLayout.findViewById<RadioButton>(R.id.approximation_set_radio)
                val inputString = inputField.text.toString()

                // Validate the input is a number between 0-10
                val intensity = inputString.toIntOrNull()
                if (intensity != null && intensity in 0..10) {
                    exercise.intensityCounter = intensity
                    exercise.rirFlag = rirCheckbox.isChecked
                    exercise.repFlag = repCheckbox.isChecked
                    exercise.topSet = topSetRadio.isChecked
                    exercise.approxSet = approximationSetRadio.isChecked

                    exerciseViewModel.modifyExercise(exercise)
                } else {
                    // Invalid input, show an error message
                    Toast.makeText(context, "Invalid intensity. Please enter a number between 0 and 10.", Toast.LENGTH_SHORT).show()
                }

                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                // User cancelled the dialog
                dialog.cancel()
            }
            .create()

        intensityDialog.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        val args = bundle?.let { ExerciseFragmentArgs.fromBundle(it) }
        val singleExercise: Exercise? = args?.singleExercise
        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
//        getLocation()
        if (!NetworkUtils.isNetworkAvailable(requireContext())) {
            AlertDialog.Builder(requireContext())
                .setTitle("No Internet Connection")
                .setMessage("You're not connected to the internet. The app will use local data.")
                .setPositiveButton(android.R.string.ok) { dialog, which ->
                    // Do something when the user acknowledges the dialog
                }
                .show()
        }

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        val exerciseViewModel = ViewModelProvider(this, ExerciseViewModelFactory())
            .get(ExerciseViewModel::class.java)
        exerciseViewModel.track(singleExercise!!)
        //change legs for the arg coming from the other view

        exerciseViewModel.exercises.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result.let {
                updateUiWithUser(it as ArrayList<Exercise>)
            }
        }

//        exerciseViewModel.cacheResults.observe(viewLifecycleOwner) { result ->
//            loadingProgressBar.visibility = View.GONE
//            result?.let {
//                updateUiWithUser(it as ArrayList<Exercise>)
//            }
//        }

//        binding.searchSportBtn.setOnClickListener {
//            val query = binding.inputSearchExercise.text.toString()
//            exerciseViewModel.searchSport(query)
//        }
        // mirar la creacion de acciones en el xml
        binding.saveButton.setOnClickListener {
            // Get the input data from the EditTexts
            val weightText = binding.weightText.text.toString()
            val repsText = binding.repsText.text.toString()
            val sdf = SimpleDateFormat("dd-mm-yyyy", Locale.getDefault())
            val currentDate = sdf.format(Date())
            // Convert to double and check for valid input
            val weight = weightText.toDoubleOrNull()
            val reps = repsText.toInt()

            if (weight == null) {
                // Show an error message because the conversion failed
                Toast.makeText(requireContext(), "Invalid input. Please enter valid numbers.", Toast.LENGTH_SHORT).show()
            } else {
                // Create a new Exercise instance with the input data
                // Make sure your Exercise class has the appropriate constructor or a factory method
                val newExercise = Exercise(
                    date = currentDate,
                    weight = weight,
                    reps = reps,
                    name = singleExercise?.name,
                    category = singleExercise?.category,
                    setNumber = exerciseViewModel.exercises_size() + 1,
                    intensityCounter = exerciseViewModel.getExercise()?.intensityCounter,
                    repFlag = exerciseViewModel.getExercise()?.repFlag,
                    rirFlag = exerciseViewModel.getExercise()?.rirFlag,
                    topSet = exerciseViewModel.getExercise()?.topSet,
                    approxSet = exerciseViewModel.getExercise()?.approxSet,
                    comments = exerciseViewModel.getExercise()?.comments,
                    restime = exerciseViewModel.getExercise()?.restime,
                    done = exerciseViewModel.getExercise()?.done,
                    )

                // Add the newExercise to a list in your ViewModel
                exerciseViewModel.addExercise(newExercise)
            }
        }
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
        binding.btnIntesity.setOnClickListener {
            if (singleExercise != null) {
                showIntensityDialog(singleExercise)
            }
        }
        binding.btnTimer.setOnClickListener {
            // cancel the current timer if it's running
            countDownTimer?.cancel()

            // set the countdown time in milliseconds
            val countDownTimeInMillis = singleExercise?.restime // e.g. 30 seconds

            countDownTimer = object : CountDownTimer(countDownTimeInMillis!!, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    // you can update a UI element here every second
                    // For example, update a TextView to show the remaining time
                    val secondsRemaining = millisUntilFinished / 1000
                    binding.timerCount.text = "$secondsRemaining s"
                }

                override fun onFinish() {
                    // the countdown is over
                    binding.timerCount.text = "Done"
                    if (singleExercise != null) {
                        singleExercise.done = true
                    }
                    if (singleExercise != null) {
                        exerciseViewModel.modifyExercise(singleExercise)
                    }
                    // You could also make a sound, start a vibration, or anything else you need
                }
            }

            countDownTimer?.start()
        }
        binding.title.text = singleExercise.name

        val weightText: EditText = binding.weightText
        val repsText: EditText = binding.repsText

// Ensure weight starts as a multiple of 5
        val weightValue: Int = if (weightText.text.toString().isNotEmpty() && weightText.text.toString().all { it.isDigit() }) {
            weightText.text.toString().toInt()
        } else {
            0
        }
        if (weightValue % 5 != 0) {
            weightText.setText("0")
        }

// Ensure reps start as an integer
        val repsValue: Int = if (repsText.text.toString().isNotEmpty() && repsText.text.toString().all { it.isDigit() }) {
            repsText.text.toString().toInt()
        } else {
            0
        }

        val plusWeightButton: Button =binding.wPlusBtn  // Replace with your actual button's ID
        val minusWeightButton: Button = binding.wMinusBtn  // Replace with your actual button's ID

        val plusRepsButton: Button = binding.rPlusBtn  // Replace with your actual button's ID
        val minusRepsButton: Button = binding.rMinusBtn  // Replace with your actual button's ID

        plusWeightButton.setOnClickListener {
            var weight = 0
            if (weightText.text.toString().all { it.isDigit() }) {
                weight = weightText.text.toString().toInt()
            }
            weight += 5
            weightText.setText(weight.toString())
        }

        minusWeightButton.setOnClickListener {
            var weight = weightText.text.toString().toInt()
            if (weight >= 5) {
                weight -= 5
                weightText.setText(weight.toString())
            }
        }

        plusRepsButton.setOnClickListener {
            var reps = 0
            if (repsText.text.toString().all { it.isDigit() }) {
                reps = repsText.text.toString().toInt()
            }
            reps += 1
            repsText.setText(reps.toString())
        }

        minusRepsButton.setOnClickListener {
            var reps = repsText.text.toString().toInt()
            if (reps >= 1) {
                reps -= 1
                repsText.setText(reps.toString())
            }
        }

    }

    private fun updateUiWithUser(model: ArrayList<Exercise>) {
        val appContext = context?.applicationContext ?: return

        val recyclerView: RecyclerView = binding.exerciseList

        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                var adapter = ExerciseItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : ExerciseItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        //BQ deporte mas popular
                        firebaseAnalytics.logEvent("Exercise"){
                            param("Exercise", model[position].name.toString())
                        }
                        Log.i("TEMP", msg)

                        val builder = AlertDialog.Builder(requireActivity())
                        //set title for alert dialog
                        builder.setTitle(model[position].name)
                        //set message for alert dialog
                        var message = "You just tapped on "+ model[position].name +"!"
                        builder.setIcon(R.drawable.munequito)

                        //performing positive action
                        //performing cancel action
                        builder.setPositiveButton("Ok"){dialogInterface , which ->

                        }


                        //findNavController().navigate(R.id.sportTeamsFragment)
//                        val sportName = model[position].name ?: ""
//                        val action = SportFragmentDirections.actionSportFragmentToSportTeamsFragment(sportName)
//
//                        findNavController().navigate(action)


//                        NavigationView.(R.id.action_sportFragment_to_sportTeamsFragment)
                    }
                })
            }
        }
    }

    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }


    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TeamsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

}