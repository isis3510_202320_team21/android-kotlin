package com.example.teamup.routine.category

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.NetworkUtils.isNetworkAvailable
import com.example.teamup.R
import com.example.teamup.databinding.FragmentCategoryBinding
import com.example.teamup.ui.team.TeamsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.ktx.Firebase
import yuku.ambilwarna.AmbilWarnaDialog
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener


class CategoryFragment : Fragment() {

    private lateinit var categoryListenerRegistration: ListenerRegistration

    private var columnCount = 1
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics


    private lateinit var mFusedLocationClient : FusedLocationProviderClient
    private val permissionId = 2

    private var msg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategoryBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
//        getLocation()

        if (!isNetworkAvailable(requireContext())) {
            AlertDialog.Builder(requireContext())
                .setTitle("No Internet Connection")
                .setMessage("You're not connected to the internet. The app will use local data.")
                .setPositiveButton(android.R.string.ok) { dialog, which ->
                    // Do something when the user acknowledges the dialog
                }
                .show()
        }

        val loadingProgressBar = binding.loading
        loadingProgressBar.visibility = View.VISIBLE

        val application = requireActivity().application
        val categoryViewModelFactory = CategoryViewModelFactory(application)
        val categoryViewModel = ViewModelProvider(this, categoryViewModelFactory).get(CategoryViewModel::class.java)
        categoryViewModel.track()

        //esta view le deberia pasar el safearg a la de ejercicio detail

//        categoryViewModel.categoryResult.observe(viewLifecycleOwner) { result ->
//            loadingProgressBar.visibility = View.GONE
////            result.error?.let {
////                showRegisterFailed(it)
////            }
//            result?.let {
//                updateUiWithUser(it)
//            }
//        }

        categoryViewModel.cacheSearchResults.observe(viewLifecycleOwner) { result ->
            loadingProgressBar.visibility = View.GONE
            result?.let {
                updateUiWithUser(it)
            }
        }

        binding.searchCategoryBtn.setOnClickListener {
            val query = binding.inputSearchCategory.text.toString()
            categoryViewModel.searchCategory(query)
        }
        // mirar la creacion de acciones en el xml
        //binding.createTeamBtn.setOnClickListener { teamViewModel.track()}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }




    }

    private fun updateUiWithUser(model: List<Category>) {
        val appContext = context?.applicationContext ?: return

        val recyclerView: RecyclerView = binding.categoryList


        Log.i("8",view.toString())
        if (recyclerView is RecyclerView) {
            with(view) {
                recyclerView.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                val adapter = CategoryItemRecyclerViewAdapter(model)
                recyclerView.adapter = adapter
                adapter.setOnItemClickListener(object : CategoryItemRecyclerViewAdapter.onItemClickListener{
                    override fun onItemClick(position: Int) {

                        //BQ categoria mas popular
                        firebaseAnalytics.logEvent("Category"){
                            param("Category", model[position].name.toString())
                        }
                        Log.i("TEMP", msg)

                        val builder = AlertDialog.Builder(requireActivity())
                        //set title for alert dialog
                        builder.setTitle(model[position].name)
                        //set message for alert dialog
                        var message = "You just tapped on "+ model[position].name +"!"
                        builder.setIcon(R.drawable.munequito)

                        //performing positive action
                        //performing cancel action
//                        builder.setPositiveButton("Ok"){dialogInterface , which ->
//
//                        }


//                          findNavController().navigate(R.id.sportTeamsFragment)
                        val categoryName = model[position].name ?: ""
                        val action = CategoryFragmentDirections.actionCategoryFragmentToExerciseListFragment(categoryName)
//
                        findNavController().navigate(action)


//                        NavigationView.(R.id.action_sportFragment_to_sportTeamsFragment)
                    }
                    override fun onOptionButtonClick(position: Int) {
                        showEditCategoryDialog(model[position],position) // Show the dialog on options button click
                    }
                })
            }
        }
    }

    private fun showEditCategoryDialog(category: Category, position: Int) {
        val application = requireActivity().application
        val categoryViewModelFactory = CategoryViewModelFactory(application)
        val categoryViewModel = ViewModelProvider(this, categoryViewModelFactory).get(CategoryViewModel::class.java)

        val dialogView = layoutInflater.inflate(R.layout.edit_category_dialog, null)
        val setColorButton: Button = dialogView.findViewById(R.id.set_color_button)
        val pickColorButton: Button = dialogView.findViewById(R.id.pick_color_button)
        val colorPicker: View = dialogView.findViewById(R.id.preview_selected_color)
        var defaultColor: Int = 0
        var colorToSet:Int = 0



        pickColorButton.setOnClickListener { // to make code look cleaner the color
            // picker dialog functionality are
            // handled in openColorPickerDialogue()
            // function
            openColorPickerDialogue(defaultColor, colorPicker)
        }

        setColorButton.setOnClickListener { // as the mDefaultColor is the global
            // variable its value will be changed as
            // soon as ok button is clicked from the
            // color picker dialog.
            colorToSet = defaultColor
        }

        val editTextCategoryName: EditText = dialogView.findViewById(R.id.editTextCategoryName)
        editTextCategoryName.setText(category.name)
        AlertDialog.Builder(requireContext())
            .setTitle("Edit Category")
            .setView(dialogView)
            .setPositiveButton("Save") { _, _ ->
                val categoryName = editTextCategoryName.text.toString()
                val updatedCategory = category.copy(name = categoryName, color = colorToSet)
                categoryViewModel.updateCategoryFb(position, category.uid!!, updatedCategory)
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
    private fun openColorPickerDialogue(defaultColor : Int, colorPreview : View){
        var defaultColor = defaultColor
        var colorPreview = colorPreview
        // the AmbilWarnaDialog callback needs 3 parameters
        // one is the context, second is default color,
        val colorPickerDialogue = AmbilWarnaDialog(requireContext(), defaultColor,
            object : OnAmbilWarnaListener {
                override fun onCancel(dialog: AmbilWarnaDialog) {
                    // leave this function body as
                    // blank, as the dialog
                    // automatically closes when
                    // clicked on cancel button
                }

                override fun onOk(dialog: AmbilWarnaDialog, color: Int) {
                    // change the mDefaultColor to
                    // change the GFG text color as
                    // it is returned when the OK
                    // button is clicked from the
                    // color picker dialog
                    defaultColor = color

                    // now change the picked color
                    // preview box to mDefaultColor
                    colorPreview.setBackgroundColor(defaultColor)
                }
            })
        colorPickerDialogue.show()
    }

    private fun checkPermissions(): Boolean {
        val appContext = context?.applicationContext ?: return false
        if (ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                appContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }


    private fun showRegisterFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TeamsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

}