package com.example.teamup.routine.exercise

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.databinding.ExerciseLayoutBinding

class ExerciseAdapter(var context:Context, var list:ArrayList<Exercise>): RecyclerView.Adapter<ExerciseAdapter.ViewHolder>() {

    class ViewHolder(itemView: ExerciseLayoutBinding):RecyclerView.ViewHolder(itemView.root){

        var set_number = itemView.setNumber
        var weight = itemView.weightLabel
        var reps = itemView.repetitionsLabel
        var checked = itemView.checkbox
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ExerciseLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // to change this
        holder.set_number.text = position.toString()
        holder.weight.text = list[position].weight.toString()
        holder.reps.text = list[position].reps.toString()
        holder.checked.isChecked = list[position].done!!

    }
}
