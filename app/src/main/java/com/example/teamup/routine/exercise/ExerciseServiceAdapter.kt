package com.example.teamup.routine.exercise

import android.content.ContentValues
import android.util.Log
import com.example.teamup.routine.category.Category
import com.example.teamup.routine.category.CategoryResultList
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import java.util.ArrayList

// class for retrieving data from the server and hopefully put it into a view of History of exercises

class ExerciseServiceAdapter {

        private var auth: FirebaseAuth = Firebase.auth
        var user: FirebaseUser? = null
        private val db = Firebase.firestore
        private val exerciseList: MutableList<Exercise> = ArrayList()

        suspend fun getExercises(): ExerciseResultList<MutableList<Exercise>> {
            var msg:String = ""
            val email = auth.currentUser?.email.toString()
            val dbRef = db.collection("categories")
//            val categoryUIDList: MutableList<String> = mutableListOf("Abs", "Biceps", "Back", "Chest", "Legs", "Cardio")

            exerciseList.clear()
            try {
                val snapshot = dbRef.get().await()
                for (data in snapshot.documents){
                    val dataid = data.id
                        val snapshot1 = dbRef.document(dataid).collection("exercises").get().await()
                        if (snapshot1 != null) {
                            for(data1 in snapshot1){
                                val exercise:Exercise? = data1.toObject(Exercise::class.java)
                                if (exercise != null) {
                                    Log.i("1", exercise.name.toString())
                                    exerciseList.add(exercise)
//                                    Log.i("1", categoryList.size.toString())
                                }
                            }
                        }
                }
                return ExerciseResultList.Success(exerciseList)
            } catch (exception: Exception){
                Log.w(ContentValues.TAG, "Error getting TEAMS documents: ", exception)
                msg = exception.message.toString()
                return ExerciseResultList.Error(Exception("Error logging in$msg"))
            }

        }
    }