package com.example.teamup.routine

import com.example.teamup.routine.exercise.Exercise
import java.util.Date

data class Routine(
    var name: String? = "",
    val username: String? = "",
    var rating: Int? = 0,
    var duration: Int? = 0,
    var done: Boolean? = false,
    var totalSets: Int? = 0,
    val date: String? = "",
    var description: String? = "",
    var exercises: List<Exercise>? = listOf(),
)