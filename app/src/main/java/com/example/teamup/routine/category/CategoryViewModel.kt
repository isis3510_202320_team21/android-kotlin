package com.example.teamup.routine.category

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teamup.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class CategoryViewModel(application: Application, val categoryRepository: CategoryRepository) : AndroidViewModel(application) {

    private var _categoryResult = MutableLiveData<List<Category>>()
    val categoryResult: LiveData<List<Category>> = _categoryResult

    private val categoryAppService = CategoryAppService()

    val cacheSearchResults:MutableLiveData<List<Category>> by lazy {
        MutableLiveData<List<Category>>()
    }

    fun searchCategory(query: String){
        val results = categoryAppService.searchCategory(query)
        cacheSearchResults.value = results
    }

    fun orderCategory(){
        val results = categoryAppService.orderExerciseByName()
        cacheSearchResults.postValue(results)
    }

    fun updateCategoryFb(pos: Int, identifier: String, category: Category) {
        viewModelScope.launch(Dispatchers.IO) {
            when(val result = categoryRepository.updateCategoryFb(identifier, category)) {
                is CategoryResultFb.Success -> {
                    result.data.let {
                        val categories = categoryAppService.getCategoryList().toMutableList()
                        categories[pos] = it
                        categoryAppService.setCategories(categories)
                        categoryAppService.setCategoriesMap(categories)
                    }
                    Log.i("6", "updateCategory: ${result.data}")
                }
                is CategoryResultFb.Error -> {
                    // Handle error
                }
            }
            val result = categoryRepository.getCategoriesFb()
            withContext(Dispatchers.Default){
                when(result){
                    is CategoryResultList.Success -> {
                        result.data?.let{
                            categoryAppService.setCategories(it)
                            categoryAppService.setCategoriesMap(it)
                            orderCategory()
                        }
                        (cacheSearchResults.value as MutableList<Category>?)?.let {
                            CategoryResultList.Success(
                                it
                            )
                        }
                    }
                    else -> result
                }
            }

        }

    }

    fun track(){
        viewModelScope.launch(Dispatchers.IO) {
            var result= categoryRepository.getCategoriesFb()
            if (result is CategoryResultList.Error){
                result = categoryRepository.getCategories()
            }
            val sortedResult = withContext(Dispatchers.Default){
                when(result){
                    is CategoryResultList.Success -> {
                        result.data?.let{
                            categoryAppService.setCategories(it)
                            categoryAppService.setCategoriesMap(it)
                            orderCategory()
                        }
                        (cacheSearchResults.value as MutableList<Category>?)?.let {
                            CategoryResultList.Success(
                                it
                            )
                        }
                    }
                    else -> result
                }
            }
            withContext(Dispatchers.Main){
                when(sortedResult){
                    is CategoryResultList.Success -> {
                        Log.i("6", "$sortedResult.data.toString() sorted dta")
                        _categoryResult.value = sortedResult.data
                        sortedResult.data.let {
                            categoryAppService.setCategories(it)
                            categoryAppService.setCategoriesMap(it)
                        }
                    }
                    else -> {
                        _categoryResult.value = listOf()
                    }
                }
            }
        }
    }
}