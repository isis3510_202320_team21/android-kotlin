package com.example.teamup.routine.category

import android.util.ArrayMap
import com.example.teamup.routine.exercise.Exercise

class CategoryAppService {
    private var categoryMap: CategoryMap = CategoryMap()
    private var categoryList: List<Category> = listOf()

    @Synchronized
    fun setCategoriesMap(category: List<Category>) {
        category.forEach { it.name?.let { it1 -> categoryMap.put(it1,
            it.exercises as MutableList<Exercise>
        ) } }
    }
    @Synchronized
    fun setCategories(category: List<Category>) {
        categoryList = category.toList() // Make a defensive copy of the list
    }

    fun getExercisesByCategory(query:String): MutableList<Exercise>? {
        return categoryMap.get(query)
    }

    fun searchCategory(query:String): List<Category>{
        return if (categoryMap.containsCategory(query)) {
            categoryList.filter { category -> category.name?.contains(query, ignoreCase=true)?: false }
        } else categoryList

    }

    fun orderExerciseByName(): List<Category>{
        val ordered = categoryList.sortedBy { category -> category.name }
        return ordered
    }

    fun getCategoryList(): List<Category>{
        return categoryList
    }
}