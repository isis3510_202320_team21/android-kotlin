package com.example.teamup.routine.category

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.databinding.ExerciseLayoutBinding
import android.graphics.Color
import android.widget.Button
import androidx.cardview.widget.CardView
import com.example.teamup.databinding.CategoryLayoutBinding


class CategoryItemRecyclerViewAdapter (private val values: List<Category>):
    RecyclerView.Adapter<CategoryItemRecyclerViewAdapter.ViewHolder>() {
    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)
        fun onOptionButtonClick(position: Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            CategoryLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = values[position]
        val ExercisesString = item.exercises?.size
        holder.category_name.text = item.name
        if (item.color > 0) {
            holder.color.setCardBackgroundColor(item.color)
        }
        else{
            holder.color.setCardBackgroundColor(Color.parseColor(item.colorString))
        }
        holder.exercise_number.text = "$ExercisesString Exercises"
//

    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: CategoryLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {

        var category_name: TextView = binding.categoryName
        var color: CardView = binding.cardViewCategory
        var exercise_number: TextView = binding.exercisesNumber
        var menuBtn: Button = binding.optionsButtonCategory

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
            menuBtn.setOnClickListener{
                listener.onOptionButtonClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + category_name.text + "'"
        }
    }
}