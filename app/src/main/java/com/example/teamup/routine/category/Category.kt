package com.example.teamup.routine.category

import android.graphics.Color
import com.example.teamup.routine.Routine
import com.example.teamup.routine.exercise.Exercise

data class Category (
    val uid: String?= "",
    val colorString: String?= "#4392F1",
    var color: Int = 0,
    var name: String?= "",
    var exercises: List<Exercise>?= listOf(),
//    var routineList: List<Routine>?= listOf()
        )
{
    init {
        color = Color.parseColor(colorString)
    }
}

