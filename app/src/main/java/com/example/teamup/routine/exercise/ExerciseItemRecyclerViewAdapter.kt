package com.example.teamup.routine.exercise

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.R
import com.example.teamup.databinding.ExerciseLayoutBinding

class ExerciseItemRecyclerViewAdapter (private val values: List<Exercise>):
    RecyclerView.Adapter<ExerciseItemRecyclerViewAdapter.ViewHolder>() {
    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ExerciseLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        val repsString = item.reps.toString()
        val weightString = item.weight.toString()

        holder.set_number.text = position.toString()
        holder.weight.text = "$weightString lbs"
        holder.reps.text = "$repsString reps"
        holder.checked.isChecked = item.done!!

    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: ExerciseLayoutBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {

        var set_number: TextView = binding.setNumber
        var weight: TextView = binding.weightLabel
        var reps: TextView = binding.repetitionsLabel
        var checked: CheckBox = binding.checkbox

        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + weight.text + "'"
        }
    }
}