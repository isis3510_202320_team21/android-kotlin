package com.example.teamup.routine.exercise

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ExerciseViewModel(val exerciseRepository: ExerciseRepository) : ViewModel() {

    private var _exercises = MutableLiveData<MutableList<Exercise>?>()
    val exercises: MutableLiveData<MutableList<Exercise>?> = _exercises

    private val exerciseAppService = ExerciseAppService()

    val cacheResults:MutableLiveData<Exercise> by lazy {
        MutableLiveData<Exercise>(
            Exercise(
                name = "Bench Press",
                category = "Chest",
                setNumber = 3,
                weight = 100.0,
                reps = 10,
                intensityCounter = 0,
                repFlag = true,
                rirFlag = false,
                comments = "This is a comment",
                restime = 60000,
                topSet = true,
                approxSet = false,
                done = false
            )
        )
    }

    fun addExercise(exercise: Exercise): ExerciseResultList<MutableList<Exercise>> {
        val result = exerciseAppService.addExercise(exercise)
        val newList = exercises.value?.toMutableList() ?: mutableListOf()
        newList.add(exercise)
        _exercises.postValue(newList)
        return result

    }
    fun exercises_size(): Int{
        return exerciseAppService.size()
    }
    fun modifyExercise(exercise: Exercise){
        cacheResults.value = exercise
    }

    fun getExercise(): Exercise?{
        return cacheResults.value
    }
    fun track(exercise: Exercise){
        viewModelScope.launch(Dispatchers.IO) {
            val result = addExercise(exercise)
            withContext(Dispatchers.Main) {
                when(result){
                    is ExerciseResultList.Success -> _exercises.value = result.data
                    else -> _exercises.value = emptyList<Exercise>().toMutableList()
                }
            }
        }
    }
}