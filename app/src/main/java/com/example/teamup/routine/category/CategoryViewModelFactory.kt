package com.example.teamup.routine.category

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.teamup.data.mapper.ModelMapper

class CategoryViewModelFactory(private val application: Application): ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryViewModel::class.java)) {
            return CategoryViewModel(application = application,
                categoryRepository = CategoryRepository(
                    firebaseSource = CategoryServiceAdapter(),
                    appContext = application,
                    modelMapper = ModelMapper()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

    //    private fun CategoryViewModel(categoryRepository: CategoryRepository): CategoryViewModel {
    //
    //    }
}