package com.example.teamup.routine.category

import android.content.ContentValues
import android.util.Log
import com.example.teamup.routine.exercise.Exercise
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import okhttp3.internal.cache.DiskLruCache.Snapshot
import java.lang.Exception
import java.util.ArrayList

class CategoryServiceAdapter {
    private var auth: FirebaseAuth = Firebase.auth
    var user: FirebaseUser? = null
    private val db = Firebase.firestore
    private val categoryList: MutableList<Category> = ArrayList()


    suspend fun getExercises(): CategoryResultList<MutableList<Category>> {
        var msg:String = ""
        val email = auth.currentUser?.email.toString()
        val dbRef = db.collection("categories")
//        val categoryUIDList: MutableList<String> = mutableListOf()
//
//        try {
//
//            val snapshot = dbRef.get().await()
//            for (document in snapshot) {
//                Log.d("1", "${document.id} => ${document.data}")
//                categoryUIDList.add(document.id)
//            }
//        } catch (e: Exception){
//            Log.w(ContentValues.TAG, "Error getting documents: ", e)
//            msg = e.message.toString()
//            return CategoryResultList.Error(Exception("Error logging in$msg"))
//        }


        categoryList.clear()
        try {
            val snapshot = dbRef.get().await()
            for (data in snapshot.documents){
                val dataid = data.id
                val category: Category? = data.toObject(Category::class.java)
                if (category != null){
                    category.name?.let { it1 -> Log.i("1", it1) }
                    categoryList.add(category)
                    val exerciseList: MutableList<Exercise> = ArrayList()
                    val snapshot1 = dbRef.document(dataid).collection("exercises").get().await()
                    if (snapshot1 != null) {
                        for(data1 in snapshot1){
                            val exercise:Exercise? = data1.toObject(Exercise::class.java)
                            if (exercise != null) {
                                Log.i("1", exercise.name.toString())
                                exerciseList.add(exercise)
                                Log.i("1", categoryList.size.toString())
                                }
                            }
                        category.exercises = exerciseList
                        }
                    }
                }
            return CategoryResultList.Success(categoryList)
        } catch (exception: Exception){
            Log.w(ContentValues.TAG, "Error getting TEAMS documents: ", exception)
            msg = exception.message.toString()
            return CategoryResultList.Error(Exception("Error logging in$msg"))
        }
    }
    suspend fun updateCategory(identifier: String, category: Category): CategoryResultFb<Category> {
        return try {
            val documentRef = db.collection("categories").document(identifier)
            val updates = mapOf(
                "name" to category.name,
            )
            val result = documentRef.update(updates).await()
            updateExerciseCategoryRefs(identifier, category)
            CategoryResultFb.Success(category)
        } catch (exception: Exception) {
            Log.w(ContentValues.TAG, "Error updating category: ", exception)
            CategoryResultFb.Error(Exception("Error updating category: ${exception.message}"))
        }
    }

    suspend fun updateExerciseCategoryRefs(categoryId: String, updatedCategory: Category) {
        val db = Firebase.firestore
        val exerciseRef = db.collection("categories").document(categoryId).collection("exercises")

        try {
            val snapshot = exerciseRef.whereEqualTo("category", categoryId).get().await()
            for (document in snapshot.documents) {
                val exercise = document.toObject(Exercise::class.java)
                exercise?.let {
                    // Here, we're assuming the field in exercise that references the category is called "categoryRef".
                    // Adjust this as per your database schema.
                    it.category = updatedCategory.name
                    // Now, update the document with the modified exercise.
                    exerciseRef.document(document.id).set(it).await()
                }
            }
        } catch (e: Exception) {
            Log.w(ContentValues.TAG, "Error updating exercise category references: ", e)
        }
    }

}