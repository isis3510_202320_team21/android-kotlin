package com.example.teamup.routine.category

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.util.Log
import com.example.teamup.TeamUp
import com.example.teamup.data.db.entities.ExerciseEntity
import com.example.teamup.data.mapper.ModelMapper
import com.example.teamup.routine.exercise.Exercise

class CategoryRepository(val firebaseSource: CategoryServiceAdapter, val appContext: Context,val modelMapper: ModelMapper) {
    suspend fun getCategoriesFb(): CategoryResultList<MutableList<Category>> {
        val result = firebaseSource.getExercises()

        if (result is CategoryResultList.Success) {
            if (result.data?.isNotEmpty() == true) {
                result.data[0].name?.let { println(it) }
            }
            return result
            // append to the list???
        } else {
            return CategoryResultList.Error(Exception("Failed to get categories"))
        }


    }
    suspend fun updateCategoryFb(uid:String, category: Category): CategoryResultFb<Category> {
        val result = firebaseSource.updateCategory(uid, category)

        if (result is CategoryResultFb.Success) {
            if (result.data != null) {
                result.data.name?.let { println(it) }
            }
            return result
            // append to the list???
        } else {
            return CategoryResultFb.Error(Exception("Failed to update categories"))
        }


    }

    suspend fun insertCategory(category: Category, exercises: List<Exercise>){
        val newCategoryEntity = modelMapper.categoryToCategoryEntity(category)
        val db = TeamUp.getDatabase(appContext)
        val categoryId:Long = db.getCategoryDAO().insertCategory(newCategoryEntity)
        val exercisesEnts = exercises.map { exercise -> modelMapper.exerciseToExerciseEntity(exercise, categoryId) }
        db.getExerciseDAO().insertExercise(exercisesEnts)
    }

    suspend fun updateCategory(category: Category){
        val db = TeamUp.getDatabase(appContext)
        val categoryEntity = modelMapper.categoryToCategoryEntity(category)
        db.getCategoryDAO().updateCategory(categoryEntity)
    }
    suspend fun deleteCategory(category: Category){
        val db = TeamUp.getDatabase(appContext)
        val categoryEntity = modelMapper.categoryToCategoryEntity(category)
        db.getCategoryDAO().deleteCategory(categoryEntity)
    }
    suspend fun deleteCategoryRoutineCrossRef(category: Category){
        val db = TeamUp.getDatabase(appContext)
        val categoryEntity = modelMapper.categoryToCategoryEntity(category)
        db.getCategoryDAO().deleteCategoryFromCrossRef(categoryEntity.category_id)
    }

    //y falta tambien las referencias a rutinas

    suspend fun getCategories(): CategoryResultList<MutableList<Category>>{
        val db = TeamUp.getDatabase(appContext)
        try {
            val categories = db.getCategoryDAO().getAllCategoryWithExercises()
            val result = categories.map{
                modelMapper.categoryEntityToCategory(it.category,
                    it.exercises as MutableList<ExerciseEntity>
                )
            }
            return CategoryResultList.Success(result as MutableList<Category>)
        } catch (e: Exception){
            Log.w(ContentValues.TAG, "Error getting CAtegories documents: ", e)
            val msg = e.message.toString()
            return CategoryResultList.Error(java.lang.Exception("Error logging in$msg"))
        }

    }

    // no tiene sentido hacerlo aca'
//    suspend fun getCategoriesWithRoutines(): List<Category> {
//        val db = TeamUp.getDatabase(appContext)
//        val categories_routines = db.getCategoryDAO().getAllCategoryWithExercises()
//        val map = mutableMapOf<Category, Routine>()
//        categories_routines.map { category_routine ->
//           modelMapper.routineEntityToRoutine(category_routine.routine,)
//        }
//    }



}