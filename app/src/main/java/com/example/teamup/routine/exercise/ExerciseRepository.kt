package com.example.teamup.routine.exercise

class ExerciseRepository(val dataSource: ExerciseServiceAdapter){
    suspend fun getExercises(): ExerciseResultList<MutableList<Exercise>> {
        val result = dataSource.getExercises()

        if (result is ExerciseResultList.Success) {
            if (result.data?.isNotEmpty() == true) {
                result.data[0].name?.let { println(it) }
            }
            return result
            // append to the list???
        } else {
            return ExerciseResultList.Error(Exception("Failed to get exercises"))
        }


    }

}