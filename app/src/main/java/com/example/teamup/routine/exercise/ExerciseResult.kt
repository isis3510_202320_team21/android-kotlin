package com.example.teamup.routine.exercise

class ExerciseResult (
    val success: Any? = null,
    val error: Int? = null)
