package com.example.teamup.routine.exercise
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Exercise (
    var date: String? = "",
    var name: String? = "",
    var category: String? = "",
    //val categoryId: Int? = 0,
    var setNumber: Int? = 0,
    var weight: Double? = 0.0,
    var reps: Int? = 0,
    var intensityCounter: Int? = 0,
    var repFlag: Boolean? = false,
    var rirFlag: Boolean? = false,
    var comments: String? = "",
    var restime: Long? = 0,
    var topSet: Boolean? = false,
    var approxSet: Boolean? = false,
    var done: Boolean? = false,
    //val routineId: Int? = 0
): Parcelable
