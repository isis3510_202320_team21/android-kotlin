package com.example.teamup.routine.category

import android.util.ArrayMap
import com.example.teamup.routine.exercise.Exercise

class CategoryMap(){
    private val internalMap = ArrayMap<String, MutableList<Exercise>>()
    fun put(category: String, exercises: MutableList<Exercise>) {
        internalMap.put(category, exercises)
    }

    fun get(category: String): MutableList<Exercise>? {
        return internalMap[category]
    }

    fun containsCategory(category: String): Boolean {
        return internalMap.containsKey(category)
    }

    fun remove(category: String) {
        internalMap.remove(category)
    }

    fun clear() {
        internalMap.clear()
    }

    fun categorySet(): Set<String> {
        return internalMap.keys
    }

    fun size(): Int {
        return internalMap.size
    }
}