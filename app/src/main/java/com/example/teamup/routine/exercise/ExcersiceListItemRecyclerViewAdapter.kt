package com.example.teamup.routine.exercise

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teamup.databinding.ExerciseLayoutBinding
import com.example.teamup.databinding.ExerciseLayoutListBinding

class ExcersiceListItemRecyclerViewAdapter (private val values: List<Exercise>):
    RecyclerView.Adapter<ExcersiceListItemRecyclerViewAdapter.ViewHolder>() {
    private lateinit var mListener: onItemClickListener
    interface onItemClickListener {

        fun onItemClick(position:Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ExerciseLayoutListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener = mListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]


        holder.set_number.text = position.toString()
        holder.name.text = item.name.toString()

    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: ExerciseLayoutListBinding, listener: onItemClickListener) : RecyclerView.ViewHolder(binding.root) {

        var set_number: TextView = binding.exerciseNumber
        var name: TextView = binding.nameLabel


        init {
            binding.root.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name.text + "'"
        }
    }
}