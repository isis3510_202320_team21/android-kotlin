package com.example.teamup.routine.exercise

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ExerciseViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExerciseViewModel::class.java)) {
            return ExerciseViewModel(
                exerciseRepository = ExerciseRepository(
                    dataSource = ExerciseServiceAdapter()
                )
            ) as T
        }
        else if (modelClass.isAssignableFrom(ExerciseListViewModel::class.java)){
            return ExerciseListViewModel(
                exerciseRepository = ExerciseRepository(
                    dataSource = ExerciseServiceAdapter()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}