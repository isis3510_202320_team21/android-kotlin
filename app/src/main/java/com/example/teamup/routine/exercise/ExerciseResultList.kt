package com.example.teamup.routine.exercise

import com.example.teamup.data.SportResultList
import com.example.teamup.sport.Sport

sealed class ExerciseResultList<out T: Any> {

    data class Success<out T:Any>(val data: MutableList<Exercise>?): ExerciseResultList<T>()
    data class Error(val exception: Exception): ExerciseResultList<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}