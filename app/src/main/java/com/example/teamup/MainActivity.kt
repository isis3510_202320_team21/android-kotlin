package com.example.teamup

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import com.example.teamup.routine.category.CategoryViewModel
import com.example.teamup.routine.category.CategoryViewModelFactory
import com.example.teamup.ui.user.MenuFragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase


class MainActivity : AppCompatActivity() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val db = FirebaseDatabase.getInstance()
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var drawerLayout: DrawerLayout

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
//
//        val categoryViewModel = ViewModelProvider(this, CategoryViewModelFactory(this.application))
//            .get(CategoryViewModel::class.java)

        val auth = Firebase.auth
        auth.currentUser?.reload()

        //TODO: For sprint 4 change findview by binding
        drawerLayout = findViewById(R.id.drawer_layout)


        val navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment_content_main )

        if(auth.currentUser!=null){
            navController.navigate(R.id.Menu)
            //val vm: View  = findViewById(R.id.Main)
            //(drawerLayout.getParent() as ViewGroup).removeView(drawerLayout)
            //replaceFragment(MenuFragment())
        }

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.hide()

        val actionBar = supportActionBar
        actionBar!!.title = ""


        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics


        val navView: NavigationView = findViewById(R.id.nav_view)

        toggle = ActionBarDrawerToggle(this,drawerLayout, R.string.open, R.string.close )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        navView.setNavigationItemSelectedListener {

            it.isChecked = true

            when(it.itemId){

                R.id.Menu -> replaceFragment(MenuFragment())

                R.id.Sports -> replaceFragment(Sports())

                R.id.Teams -> replaceFragment(Teams())

                R.id.nav_logout -> {
                    auth.signOut()
                    supportActionBar?.hide()
                    val intent= Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()}
                    //replaceFragment(Main())}


                //R.id.nav_graphs -> {
                //    replaceFragment( GraphsFragment())
                //}
            }
            true
        }

        auth.currentUser?.reload()
        val textview: TextView = navView.getHeaderView(0).findViewById(R.id.user_name) as TextView
        textview.setText(auth.currentUser?.displayName)

        val textview_email: TextView = navView.getHeaderView(0).findViewById(R.id.user_email) as TextView
        textview_email.setText(auth.currentUser?.email)

        firebaseAnalytics.logEvent("Android_Version") {
            param("number", androidVersion().toString())
        }
    }


    private fun replaceFragment(fragment: Fragment){
        val fragmentM = supportFragmentManager
        val transaction = fragmentM.beginTransaction()
        transaction.replace(R.id.nav_host_fragment_content_main,fragment)
        transaction.commit()
        drawerLayout.closeDrawers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(toggle.onOptionsItemSelected(item)){

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        var view: View? = findViewById(R.id.Menu)
        if(view != null && view.isVisible){
            moveTaskToBack(true)
        }
        else{
            super.onBackPressed()
        }
    }

    fun androidVersion (): Int {
        return android.os.Build.VERSION.SDK_INT
    }

}