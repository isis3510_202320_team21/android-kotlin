package com.example.teamup

import android.R.attr.searchIcon
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.FragmentTeamsBinding


/**
 * A simple [Fragment] subclass.
 * Use the [Teams.newInstance] factory method to
 * create an instance of this fragment.
 */
class Teams : Fragment() {
    // TODO: Rename and change types of parameters

    private var _binding: FragmentTeamsBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTeamsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.createTeamBtn.setOnClickListener { findNavController().navigate(R.id.action_teams_toCreateTeam)}
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}