package com.example.teamup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.teamup.databinding.FragmentCreateTeamBinding


/**
 * A simple [Fragment] subclass.
 * Use the [CreateTeam.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateTeam : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentCreateTeamBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateTeamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.locationBtn.setOnClickListener { findNavController().navigate(R.id.action_createTeam_toSearchStages) }
        binding.munequitoImageButton.setOnClickListener { findNavController().navigate(R.id.Menu) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}