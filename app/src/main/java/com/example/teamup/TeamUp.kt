package com.example.teamup

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.teamup.data.db.RoutineDatabase

class TeamUp: Application() {
    companion object {
        @Volatile private var instance: RoutineDatabase? = null

        fun getDatabase(context: Context): RoutineDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                RoutineDatabase::class.java, "MyDatabase-prueba1.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}